package kr.co.pyj246.webbase.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;

@Aspect
public class MapperMethodInvokeAspect {

    @Around("execution(public * kr.co.pyj246.webbase.persistence.*Mapper.*(..))")
    public Object redoWhenPackageRebuildException(ProceedingJoinPoint pjp) throws Throwable {
        Object ret;
        ret = pjp.proceed();
        return ret;
    }
}
