package kr.co.pyj246.webbase.message;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.AbstractMessageSource;

import java.net.URISyntaxException;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import kr.co.pyj246.webbase.component.ConvertManager;
import kr.co.pyj246.webbase.model.base.message.Language;
import kr.co.pyj246.webbase.model.base.message.Message;
import kr.co.pyj246.webbase.model.base.message.MessagePack;
import kr.co.pyj246.webbase.persistence.BaseMapper;

/**
 * 다국어 지원을 위한 Message 로드 및 FreeMarker Model이 될수 있도록하는 Class.
 * 
 * @author yjpark
 *
 */
public class DatabaseMessageSource extends AbstractMessageSource implements InitializingBean {

    private static final Logger LOGGER = LoggerFactory.getLogger(DatabaseMessageSource.class);
    
    @Autowired
    private ConvertManager convertManager;
    
    @Autowired
    private BaseMapper baseMapper;
    
    private final Map<String, MessagePack> messages = new HashMap<String, MessagePack>();

    private final Map<String, MessageFormat> cachedMessageFormats = new HashMap<String, MessageFormat>();

    @Override
    protected String resolveCodeWithoutArguments(String code, Locale locale) {
        String language = locale.getLanguage();
        String message = null;
        if (messages.containsKey(language)) {
            synchronized (messages) {
            	MessagePack languagePack = messages.get(language);
                if (!languagePack.getMessages().containsKey(code)) {
                    // reload
                    try {
                        reloadLanguagePack(language, languagePack);
                    } catch (URISyntaxException e) {
                        LOGGER.error("LanguagePack Load Fail: " + language, e);
                    }
                }
                message = languagePack.getMessages().get(code);
            }
        }
        return message;
    }

    @Override
    protected MessageFormat resolveCode(String code, Locale locale) {
        String formatKey = code + "-" + locale.getLanguage();
        String message = resolveCodeWithoutArguments(code, locale);
        if (message == null) {
            return null;
        }
        synchronized (cachedMessageFormats) {
            MessageFormat messageFormat = this.cachedMessageFormats.get(formatKey);
            if (messageFormat == null) {
                messageFormat = createMessageFormat(message, locale);
                this.cachedMessageFormats.put(formatKey, messageFormat);
            }
            return messageFormat;
        }
    }

    @Override
    protected String getMessageInternal(String code, Object[] args, Locale locale) {
        Locale checkedLocale;
        if (locale == null) {
            checkedLocale = LocaleContextHolder.getLocale();
        } else {
            checkedLocale = locale;
        }
        return super.getMessageInternal(code, args, checkedLocale);
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        try {
            loadLanguagePacks();
        } catch (URISyntaxException e) {
            LOGGER.error("Fail to Load Language Pack", e);
        }
    }
    
    private void loadLanguagePacks() throws URISyntaxException {
    	Map<String, MessagePack> packs = new HashMap<String, MessagePack>();
    	
    	// 언어별 Data 조회
    	Map<String, String> messageMap = null;
    	for(Language language : Language.values()) {
    		messageMap = this.getMessageMap(language.toString());
    		if(messageMap != null && messageMap.size() > 0) {
    			MessagePack messagePack = new MessagePack();
    			messagePack.setLanguage(language.toString());
    			messagePack.setMessages(messageMap);
    			packs.put(language.toString(), messagePack);
    		}
    	}
    	
    	messages.clear();
        messages.putAll(packs);
    }
    
    private void reloadLanguagePack(String language, MessagePack messagePack) throws URISyntaxException {
    	Map<String, String> messageMap = this.getMessageMap(language);
		if(messageMap != null && messageMap.size() > 0) {
			messagePack.setLanguage(language.toString());
			messagePack.setMessages(messageMap);
		}
    }
    
    private Map<String, String> getMessageMap(String language) {
    	Map<String, String> messageMap = new HashMap<String, String>();
    	for(Message message : baseMapper.selectMessageListByLanguage(language)) {
    		messageMap.put(message.getId(), message.getMessage());
    	}
    	return messageMap;
    }
    
    public Map<String, String> getAllMessages(String language) {
    	Map<String, String> allMessages = null;
    	if(messages.containsKey(language)) {
    		allMessages = messages.get(language).getMessages();
    	};
    	return allMessages;
    }
}

