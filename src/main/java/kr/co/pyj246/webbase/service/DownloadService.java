package kr.co.pyj246.webbase.service;

import java.util.List;

import org.apache.poi.ss.usermodel.Workbook;

public interface DownloadService {
	
	public List<List<String>> getTestCsvContents();
	
	public Workbook getTestWorkbook();
	
	public String getTestPdfHtml();
	
}
