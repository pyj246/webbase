package kr.co.pyj246.webbase.service.impl;

import java.util.Map;

import kr.co.pyj246.webbase.component.ConvertManager;
import kr.co.pyj246.webbase.service.EditService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EditServiceImpl implements EditService {

	@Autowired
	private ConvertManager convertManager;
	
	public void setEditData(String dataJson, String userId) {
		Map<String, Object> data = convertManager.convertJsonStringIntoMap(dataJson);
		for(String key : data.keySet()) {
			System.out.print(key);
			System.out.print("\t → ");
			System.out.println(data.get(key));
		}
		System.out.println("======================================");
		System.out.println("");
	}
}
