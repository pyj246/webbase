package kr.co.pyj246.webbase.service.impl;

import java.util.ArrayList;
import java.util.List;

import kr.co.pyj246.webbase.component.ExcelManager;
import kr.co.pyj246.webbase.model.base.excel.PoiType;
import kr.co.pyj246.webbase.service.DownloadService;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DownloadServiceImpl implements DownloadService {
	
	@Autowired
	private ExcelManager excelManager;
	
	@Override
	public List<List<String>> getTestCsvContents() {
		
		List<List<String>> contents = new ArrayList<List<String>>();
		
		List<String> content = new ArrayList<String>();
		content.add("1-1");
		content.add("1-2");
		content.add("1-3");
		content.add("1-4");
		content.add("1-5");
		contents.add(content);
		
		content = new ArrayList<String>();
		content.add("2-1");
		content.add("2-2");
		content.add("2-3");
		content.add("2-4");
		content.add("2-5");
		contents.add(content);
		
		content = new ArrayList<String>();
		content.add("3-1");
		content.add("3-2");
		content.add("3-3");
		content.add("3-4");
		content.add("3-5");
		contents.add(content);
		
		content = new ArrayList<String>();
		content.add("4-1");
		content.add("4-2");
		content.add("4-3");
		content.add("4-4");
		content.add("4-5");
		contents.add(content);
		
		return contents;
	}
	
	@Override
	public Workbook getTestWorkbook() {
		
		Workbook workbook = excelManager.createWorkbook(PoiType.HSSF);
		workbook.createSheet("testSheet");
		
		return workbook;
	}
	
	@Override
	public String getTestPdfHtml() {
		StringBuilder stringBuilder = new StringBuilder();
		
		stringBuilder.append("<html>");
		stringBuilder.append("<head>");
		stringBuilder.append("</head>");
		stringBuilder.append("<body>");
		stringBuilder.append("<table><tr><td width=\"80%\">test</td><td>test2</td></tr></table>");
		stringBuilder.append("</body>");
		stringBuilder.append("</html>");
		
		return stringBuilder.toString();
	}
	
}
