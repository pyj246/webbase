package kr.co.pyj246.webbase.exception;

public class MessageException extends RuntimeException implements DefinedException {

	private static final long serialVersionUID = 1L;
	
	private String errorCode;
	
	public MessageException(String string) {
		super(string);
	}
	
	public MessageException(String string, String errorCode) {
		super(string);
		this.errorCode = errorCode;
	}

	public String getErrorCode() {
		return errorCode;
	}

}