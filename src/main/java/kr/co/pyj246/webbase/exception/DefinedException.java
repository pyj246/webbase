package kr.co.pyj246.webbase.exception;

/**
 * Defined Exception 을 찾기 위한 Marker Interface.
 * 
 * @author yjpark
 */
public interface DefinedException {}
