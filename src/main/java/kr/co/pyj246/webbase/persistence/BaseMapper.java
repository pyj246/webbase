package kr.co.pyj246.webbase.persistence;

import java.util.List;

import kr.co.pyj246.webbase.model.base.message.Message;

public interface BaseMapper {
	
	List<Message> selectMessageListByLanguage(String locale);
	
}
