package kr.co.pyj246.webbase.persistence.typeHandler;

import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedJdbcTypes;
import org.apache.ibatis.type.MappedTypes;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@MappedJdbcTypes(JdbcType.VARCHAR)
@MappedTypes(Boolean.class)
public class BooleanCharTypeHandler extends BaseTypeHandler<Boolean> {

    @Override
    public void setNonNullParameter(PreparedStatement ps, int i,
            Boolean parameter, JdbcType jdbcType) throws SQLException {
        if (parameter.equals(Boolean.TRUE)) {
            ps.setString(i, "Y");
        } else {
            ps.setString(i, "N");
        }
    }

    @Override
    public Boolean getNullableResult(ResultSet rs, String columnName)
            throws SQLException {
        return convertStringToBoolean(rs.getString(columnName));
    }

    @Override
    public Boolean getNullableResult(ResultSet rs, int columnIndex)
            throws SQLException {
        return convertStringToBoolean(rs.getString(columnIndex));
    }

    @Override
    public Boolean getNullableResult(CallableStatement cs, int columnIndex)
            throws SQLException {
        return convertStringToBoolean(cs.getString(columnIndex));
    }

    private Boolean convertStringToBoolean(String source) {
        if (source != null && "Y".equals(source)) {
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }
}