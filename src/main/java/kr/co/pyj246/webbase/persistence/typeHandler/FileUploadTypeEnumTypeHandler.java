package kr.co.pyj246.webbase.persistence.typeHandler;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import kr.co.pyj246.webbase.model.file.FileUploadType;

import org.apache.ibatis.type.EnumTypeHandler;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedJdbcTypes;
import org.apache.ibatis.type.MappedTypes;

@MappedJdbcTypes(JdbcType.CHAR)
@MappedTypes(FileUploadType.class)
public class FileUploadTypeEnumTypeHandler extends EnumTypeHandler<FileUploadType> {
	
	public FileUploadTypeEnumTypeHandler() {
		super(FileUploadType.class);
	}
	
	@Override
	public void setNonNullParameter(PreparedStatement ps, int i,
			FileUploadType parameter, JdbcType jdbcType) throws SQLException {
		ps.setString(i, parameter.getCode());
	}
	
	@Override
	public FileUploadType getNullableResult(ResultSet rs, String columnName) throws SQLException {
		String s = rs.getString(columnName);
		return FileUploadType.valueOfCode(s);
	}

	@Override
	public FileUploadType getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
		String s = rs.getString(columnIndex);
		return FileUploadType.valueOfCode(s);
	}

	@Override
	public FileUploadType getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
		String s = cs.getString(columnIndex);
		return FileUploadType.valueOfCode(s);
	}
	
}
