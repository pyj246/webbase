package kr.co.pyj246.webbase.persistence.typeHandler;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import kr.co.pyj246.webbase.model.base.DataState;

import org.apache.ibatis.type.EnumTypeHandler;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedJdbcTypes;
import org.apache.ibatis.type.MappedTypes;

@MappedJdbcTypes(JdbcType.CHAR)
@MappedTypes(DataState.class)
public class DataStateEnumTypeHandler extends EnumTypeHandler<DataState> {

	public DataStateEnumTypeHandler() {
		super(DataState.class);
	}
	
	@Override
	public void setNonNullParameter(PreparedStatement ps, int i,
			DataState parameter, JdbcType jdbcType) throws SQLException {
		ps.setString(i, parameter.getCode());
	}
	
	@Override
	public DataState getNullableResult(ResultSet rs, String columnName) throws SQLException {
		String s = rs.getString(columnName);
		return DataState.valueOfCode(s);
	}

	@Override
	public DataState getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
		String s = rs.getString(columnIndex);
		return DataState.valueOfCode(s);
	}

	@Override
	public DataState getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
		String s = cs.getString(columnIndex);
		return DataState.valueOfCode(s);
	}
}
