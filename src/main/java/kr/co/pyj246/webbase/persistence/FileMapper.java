package kr.co.pyj246.webbase.persistence;

import org.apache.ibatis.annotations.Param;

import kr.co.pyj246.webbase.model.base.DataState;
import kr.co.pyj246.webbase.model.file.FileDownload;
import kr.co.pyj246.webbase.model.file.FileInfo;

public interface FileMapper {
	
	FileInfo selectFile(@Param("key") int key);
	
	void insertFile(FileInfo fileInfo);
	
	void updateFileStatus(@Param("key") int key, @Param("state") DataState state, @Param("userKey") int userKey);
	
	FileDownload selectFileDownload(@Param("key") int key);
	
	void insertFileDownload(FileDownload fileDownload);
	
	void updateFileDownloadSize(@Param("key") int key, @Param("size") long size, @Param("userKey") int userKey);
	
	void updateFileDownloadStatus(@Param("key") int key, @Param("state") DataState state, @Param("userKey") int userKey);
}
