package kr.co.pyj246.webbase.persistence;

import kr.co.pyj246.webbase.model.user.UserDetail;

public interface UserMapper {
	UserDetail selectUserDetail(String userId);
}
