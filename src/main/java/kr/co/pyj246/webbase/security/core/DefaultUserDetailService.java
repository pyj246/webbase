package kr.co.pyj246.webbase.security.core;

import java.util.ArrayList;
import java.util.List;

import kr.co.pyj246.webbase.model.user.UserDetail;
import kr.co.pyj246.webbase.persistence.UserMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public class DefaultUserDetailService implements UserDetailsService {
	
	@Autowired
	private UserMapper userMapper;
	
	public UserDetails loadUserByUsername(String userId) throws UsernameNotFoundException {
		UserDetail user = userMapper.selectUserDetail(userId);
		
		if(user == null) {
			throw new UsernameNotFoundException(String.format("사용자[%s] 정보가 존재하지 않습니다.", userId));
		}
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		
		authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
		
		return new User(user.getEmail(), user.getPassword(), true, true, true, true, authorities);
	}
}
