package kr.co.pyj246.webbase.security.core;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.security.core.session.SessionDestroyedEvent;
import org.springframework.security.core.session.SessionInformation;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.util.Assert;

public class EhcacheSessionRegistryImpl implements SessionRegistry, ApplicationListener<SessionDestroyedEvent> {

	private Cache principals;
	
	private Cache sessionIds;
	
	@Autowired
	public void SetCacheManger(CacheManager cacheManager) {
		principals = cacheManager.getCache("principals");
		sessionIds = cacheManager.getCache("sessionIds");
	}
	
	@Override
	public void onApplicationEvent(SessionDestroyedEvent event) {
		String sessionId = event.getId();
        removeSessionInformation(sessionId);
	}
	
	@Override
	public void refreshLastRequest(String sessionId) {
		Assert.hasText(sessionId, "SessionId required as per interface contract");

        SessionInformation info = getSessionInformation(sessionId);

        if(info != null) {
            info.refreshLastRequest();
        }
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void registerNewSession(String sessionId, Object principal) {
		Assert.hasText(sessionId, "SessionId required as per interface contract");
        Assert.notNull(principal, "Principal required as per interface contract");

        if(getSessionInformation(sessionId) != null) {
            removeSessionInformation(sessionId);
        }

        sessionIds.put(new Element(sessionId,
                new SessionInformation(principal, sessionId, new Date())));

        Element sessionsUsedByPrincipalElement = principals.get(principal);

        List<String> sessionsUsedByPrincipal = null;

        if(sessionsUsedByPrincipalElement != null) {
            Object elementObject = sessionsUsedByPrincipalElement.getObjectValue();
            if (elementObject != null) {
                sessionsUsedByPrincipal = (List<String>) elementObject;
            }
        }

        if(sessionsUsedByPrincipal == null) {
            sessionsUsedByPrincipal = new ArrayList<String>();
        }
        sessionsUsedByPrincipal.add(sessionId);

        principals.put(new Element(principal, sessionsUsedByPrincipal));
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void removeSessionInformation(String sessionId) {
		Assert.hasText(sessionId, "SessionId required as per interface contract");
		
		SessionInformation sessionInfomation = getSessionInformation(sessionId);
		
		if(sessionInfomation == null) {
			return;
		}
		
		for(Object key : sessionIds.getKeys()) {
			if(key instanceof String && key.equals(sessionId)) {
				sessionIds.remove(key);
				break;
			}
		}
		
		Element sessionsUsedByPrincipalElement = principals.get(sessionInfomation.getPrincipal());
		
		if(sessionsUsedByPrincipalElement == null) {
			return;
		} else if(sessionsUsedByPrincipalElement.getObjectValue() == null) {
			principals.remove(sessionInfomation.getPrincipal());
			return;
		} 
		
		List<String> sessionsUsedByPrincipal = (List<String>) sessionsUsedByPrincipalElement.getObjectValue();
		
		List<String> removeSessionIdObjects = new ArrayList<String>();
		for(String id : sessionsUsedByPrincipal) {
			if(id.equals(sessionId)) {
				removeSessionIdObjects.add(id);
			}
		}
		
		sessionsUsedByPrincipal.removeAll(removeSessionIdObjects);
		
		if(sessionsUsedByPrincipal.isEmpty()) {
			principals.remove(sessionInfomation.getPrincipal());
		}
	}
	
	@Override
	public SessionInformation getSessionInformation(String sessionId) {
		Assert.hasText(sessionId, "SessionId required as per interface contract");
		
		SessionInformation sessionInformation = null;
		Element element = null;
		
		for(Object key : sessionIds.getKeys()) {
			if(key instanceof String && key.equals(sessionId)) {
				element = sessionIds.get(key);
				break;
			}
		}
		
		if(element != null) {
			Object object = element.getObjectValue();
			if(object instanceof SessionInformation) {
				sessionInformation = (SessionInformation) object;
			}
		}
		
		return sessionInformation;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SessionInformation> getAllSessions(Object principal,
			boolean includeExpiredSessions) {
		Element sessionsUsedByPrincipalElement = principals.get(principal);
		
		if(sessionsUsedByPrincipalElement == null || sessionsUsedByPrincipalElement.getObjectValue() == null) {
            return Collections.emptyList();
        }
		
		Object elementObject = sessionsUsedByPrincipalElement.getObjectValue();
		final List<String> sessionsUsedByPrincipal = (List<String>) elementObject;
		
		List<SessionInformation> list = new ArrayList<SessionInformation>(sessionsUsedByPrincipal.size());
		
		for(String sessionId : sessionsUsedByPrincipal) {
            SessionInformation sessionInformation = getSessionInformation(sessionId);

            if(sessionInformation == null) {
                continue;
            }

            if(includeExpiredSessions || !sessionInformation.isExpired()) {
                list.add(sessionInformation);
            }
        }
		
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Object> getAllPrincipals() {
		return principals.getKeys();
	}
	
}
