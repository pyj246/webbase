package kr.co.pyj246.webbase.security.core;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import kr.co.pyj246.webbase.exception.MessageException;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.session.SessionInformation;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.web.authentication.session.SessionAuthenticationException;
import org.springframework.security.web.authentication.session.SessionAuthenticationStrategy;
import org.springframework.util.Assert;

public class CacheAuthenticationStrategy implements SessionAuthenticationStrategy {
	
	private final SessionRegistry sessionRegistry;
	
    private int maximumSessions;
	
    public CacheAuthenticationStrategy(SessionRegistry sessionRegistry, int maximumSessions) {
    	 Assert.notNull(sessionRegistry, "The sessionRegistry cannot be null");
         this.sessionRegistry = sessionRegistry;
         this.maximumSessions = maximumSessions;
	}
    
	@Override
	public void onAuthentication(Authentication authentication,
			HttpServletRequest request, HttpServletResponse response)
			throws SessionAuthenticationException {
		
		final List<SessionInformation> sessions = sessionRegistry.getAllSessions(authentication.getPrincipal(), false);
		
        if (maximumSessions == -1 || sessions == null || sessions.size() < maximumSessions) {
            return;
        } else {
        	 HttpSession session = request.getSession(false);

             if (session != null) {
                 for (SessionInformation si : sessions) {
                     if (si.getSessionId().equals(session.getId())) {
                         return;
                     }
                 }
             }
             
             throw new MessageException("Duplication Login");
        }
	}
	
}
