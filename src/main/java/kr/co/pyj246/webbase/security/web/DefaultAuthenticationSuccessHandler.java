package kr.co.pyj246.webbase.security.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import kr.co.pyj246.webbase.constant.DefaultConstant;
import kr.co.pyj246.webbase.model.user.UserDetail;
import kr.co.pyj246.webbase.persistence.UserMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;

public class DefaultAuthenticationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {
	
	private String defaultSuccessUrl;
	
	@Autowired
	private UserMapper userMapper;
	
	private RequestCache requestCache = new HttpSessionRequestCache();
	
	@Override
	public void onAuthenticationSuccess(HttpServletRequest request,
			HttpServletResponse response, Authentication authentication)
			throws IOException, ServletException {
		
		// 사용자 정보 조회 및 패스워드 정보 삭제
		UserDetail user = userMapper.selectUserDetail(authentication.getName());
		user.setPassword(null);
		
		// 사용자 정보를 세션에 저장
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute(DefaultConstant.SESSION_USER, user);
		
		// redirectUrl URL
		String redirectUrl = null;
		SavedRequest savedRequest = requestCache.getRequest(request, response);
		if(savedRequest != null) {
			requestCache.removeRequest(request, response);
			redirectUrl = savedRequest.getRedirectUrl();
		}
		
		if(httpSession != null) {
			if(redirectUrl == null || redirectUrl.length() == 0) {
				redirectUrl = "/";
			}
		}
		
		if (httpSession != null) {
            request.getSession().setAttribute(DefaultConstant.AUTHENTICATION_REDIRECT_URL, redirectUrl);
        }
		
		super.setDefaultTargetUrl(defaultSuccessUrl);
		super.onAuthenticationSuccess(request, response, authentication);
	}
	
	public void setDefaultSuccessUrl(String defaultSuccessUrl) {
		this.defaultSuccessUrl = defaultSuccessUrl;
	}
}
