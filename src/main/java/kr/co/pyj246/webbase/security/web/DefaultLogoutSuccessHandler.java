package kr.co.pyj246.webbase.security.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.SimpleUrlLogoutSuccessHandler;
import org.springframework.security.web.util.matcher.ELRequestMatcher;

public class DefaultLogoutSuccessHandler extends SimpleUrlLogoutSuccessHandler {

    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response,
            Authentication authentication) throws IOException, ServletException {
    	if((new ELRequestMatcher("hasHeader('X-Requested-With','XMLHttpRequest')")).matches(request)) {
    		response.setContentType("application/json");
			response.getWriter().write("{\"redirectUrl\":\"/\"}");
    	} else {
    		super.onLogoutSuccess(request, response, authentication);
    	}
    }
}
