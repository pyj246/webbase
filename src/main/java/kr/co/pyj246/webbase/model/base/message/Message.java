package kr.co.pyj246.webbase.model.base.message;

import kr.co.pyj246.webbase.model.base.PaginatedResultBase;

public class Message extends PaginatedResultBase {
	
	private String id;
	
	private String message;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
