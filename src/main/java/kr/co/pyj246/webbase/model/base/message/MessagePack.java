package kr.co.pyj246.webbase.model.base.message;

import java.util.Map;

public class MessagePack {
	
	private String language;
	
    private Map<String, String> messages;

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public Map<String, String> getMessages() {
		return messages;
	}

	public void setMessages(Map<String, String> messages) {
		this.messages = messages;
	}

	@Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("MessagePack{");
        sb.append("language=").append(language);
        sb.append(", message=").append(messages);
        sb.append('}');
        return sb.toString();
    }
}
