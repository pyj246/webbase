package kr.co.pyj246.webbase.model.base;

public enum DataState {
	Active("A"), Delete("D"), Hidden("H"), Success("S"), Fail("F"), Expire("E"), Ongoing("O"), Cancel("C");
	
	private String code;
	
	DataState(String code) {
		this.code = code;
	}
	
	public String getCode() {
		return this.code;
	}
	
	public static DataState valueOfCode(String code) {
		for(DataState dataStatus :DataState.values()) {
			if(dataStatus.getCode().equals(code)) {
				return dataStatus;
			}
		}
		return null;
	}
}
