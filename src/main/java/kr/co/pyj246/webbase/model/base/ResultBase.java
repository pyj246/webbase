package kr.co.pyj246.webbase.model.base;

public class ResultBase {
	
	private int key;

	private DataState state;

    private String inputTime;
    
    private int inputUserKey;

	public int getKey() {
		return key;
	}

	public void setKey(int key) {
		this.key = key;
	}

	public DataState getState() {
		return state;
	}

	public void setState(DataState state) {
		this.state = state;
	}

	public String getInputTime() {
		return inputTime;
	}

	public void setInputTime(String inputTime) {
		this.inputTime = inputTime;
	}

	public int getInputUserKey() {
		return inputUserKey;
	}

	public void setInputUserKey(int inputUserKey) {
		this.inputUserKey = inputUserKey;
	}

}
