package kr.co.pyj246.webbase.model.base.excel;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * Poi Type
 */
public enum PoiType {
	HSSF("xls"), XSSF("xlsx"), SXSSF("xlsx");
	
	private String extension;
	
	private PoiType(String extension) {
		this.extension = extension;
	}
	
	public String getExtension() {
		return extension;
	}
	
	public static PoiType valueOfExtension(String fileName) {
		if(fileName.endsWith(".xls")) {
			return HSSF;
		} else if(fileName.endsWith(".xlsx")) {
			return PoiType.XSSF;
		} else {
			return null;
		}
	}
	
	public static PoiType valueOfWorkbook(Workbook workbook) {
		if(workbook instanceof HSSFWorkbook) {
			return HSSF;
		}
		if(workbook instanceof XSSFWorkbook) {
			return XSSF;
		}
		if(workbook instanceof SXSSFWorkbook) {
			return SXSSF;
		}
		return null;
	}
}
