package kr.co.pyj246.webbase.model.file;

public enum FileUploadType {
	
	SYSTEM("S", "system"),
	REPORT("R", "report"),
	UPLOAD("U", "upload");
	
	private String code;
	
	private String directory;
	
	private FileUploadType(String code, String directory) {
		this.code = code;
		this.directory = directory;
	}
	
	public String getCode() {
		return this.code;
	}
	
	public String getDirectory() {
		return this.directory;
	}
	
	public static FileUploadType valueOfCode(String code) {
		for(FileUploadType fileUploadType :FileUploadType.values()) {
			if(fileUploadType.getCode().equals(code)) {
				return fileUploadType;
			}
		}
		return null;
	}
	
	public static FileUploadType valueOfDirectory(String directory) {
		for(FileUploadType fileUploadType :FileUploadType.values()) {
			if(fileUploadType.getDirectory().equals(directory)) {
				return fileUploadType;
			}
		}
		return null;
	}
	
}
