package kr.co.pyj246.webbase.model.base;

import java.util.Map;

public class ListParamBase {
	
	private boolean isPaginatedList;
	
	private int pageSize;
	
	private int pageIndex;
	
	private String ordering;
	
	private Map<String, String> filters; 
	
	public boolean isPaginatedList() {
		return isPaginatedList;
	}

	public void setPaginatedList(boolean isPaginatedList) {
		this.isPaginatedList = isPaginatedList;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getPageIndex() {
		return pageIndex;
	}

	public void setPageIndex(int pageIndex) {
		this.pageIndex = pageIndex;
	}
	
	public String getOrdering() {
		return ordering;
	}

	public void setOrdering(String ordering) {
		this.ordering = ordering;
	}

	public Map<String, String> getFilters() {
		return filters;
	}
	
	public void setFilters(Map<String, String> filters) {
		this.filters = filters;
	}
	
}
