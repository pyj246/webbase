package kr.co.pyj246.webbase.model.file;

import kr.co.pyj246.webbase.model.base.PaginatedResultBase;

public class FileInfo extends PaginatedResultBase {
	
	private String name;
	
	private String extension;
	
	private String path;
	
	private String contentType;
	
	private FileUploadType uploadType;
	
	private long size;
	
	private int userKey;
	
	private String createTime;
	
	private String exprieTime;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public FileUploadType getUploadType() {
		return uploadType;
	}

	public void setUploadType(FileUploadType uploadType) {
		this.uploadType = uploadType;
	}

	public long getSize() {
		return size;
	}

	public void setSize(long size) {
		this.size = size;
	}

	public int getUserKey() {
		return userKey;
	}

	public void setUserKey(int userKey) {
		this.userKey = userKey;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getExprieTime() {
		return exprieTime;
	}

	public void setExprieTime(String exprieTime) {
		this.exprieTime = exprieTime;
	}
	
}