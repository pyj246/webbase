package kr.co.pyj246.webbase.model.file;

import kr.co.pyj246.webbase.model.base.PaginatedResultBase;

public class FileDownload extends PaginatedResultBase {
	
	private int fileKey;
	
	private int userKey;
	
	private long size;
	
	private String startTime;
	
	private String endTime;

	public int getFileKey() {
		return fileKey;
	}

	public void setFileKey(int fileKey) {
		this.fileKey = fileKey;
	}

	public int getUserKey() {
		return userKey;
	}

	public void setUserKey(int userKey) {
		this.userKey = userKey;
	}

	public long getSize() {
		return size;
	}

	public void setSize(long size) {
		this.size = size;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

}
