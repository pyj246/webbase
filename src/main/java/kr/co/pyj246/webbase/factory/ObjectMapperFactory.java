package kr.co.pyj246.webbase.factory;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.beans.factory.FactoryBean;

/**
 * @file ObjectMapperFactory.java 
 * @time 오후 3:02:49
 * @author yjpark
 */
public class ObjectMapperFactory implements FactoryBean<ObjectMapper> {

	@Override
    public ObjectMapper getObject() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_CONTROL_CHARS, true);
        objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        return objectMapper;
    }

	@Override
    public Class<?> getObjectType() {
        return ObjectMapper.class;
    }

	@Override
    public boolean isSingleton() {
        return false;
    }
}
