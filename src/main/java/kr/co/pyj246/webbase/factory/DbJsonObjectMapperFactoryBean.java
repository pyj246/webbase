package kr.co.pyj246.webbase.factory;

import org.springframework.beans.factory.FactoryBean;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;


public class DbJsonObjectMapperFactoryBean implements FactoryBean<ObjectMapper> {
	
	private ObjectMapper objectMapper;
	
	@Override
	public ObjectMapper getObject() throws Exception {
		this.objectMapper = new ObjectMapper();
		this.objectMapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
		this.objectMapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_CONTROL_CHARS, true);
		this.objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
		this.objectMapper.enable(DeserializationFeature.READ_ENUMS_USING_TO_STRING);
		this.objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
		
		return this.objectMapper;
	}

	@Override
	public Class<?> getObjectType() {
		return ObjectMapper.class;
	}

	@Override
	public boolean isSingleton() {
		return false;
	}
}
