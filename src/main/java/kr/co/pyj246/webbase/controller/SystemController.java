package kr.co.pyj246.webbase.controller;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.http.HttpSession;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.session.SessionInformation;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/system")
public class SystemController {
	
	@Autowired
	private CacheManager cacheManager;
	
	@Autowired
	private SessionRegistry sessionRegistry;
	
	public String ehcache() {
		return "system/ehcache";
	}
	
	public String ehcacheList(HttpSession httpSession) {
		Cache pt = cacheManager.getCache("principals");
		Cache st = cacheManager.getCache("sessionIds");
		List aa = pt.getKeys();
		List bb = st.getKeys();
		
		String ip = null;
		try{
		    for(Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements();) {
		        NetworkInterface intf = en.nextElement();
		        for(Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements();) {
		            InetAddress inetAddress = enumIpAddr.nextElement();
		            if(!inetAddress.isLoopbackAddress() && !inetAddress.isLinkLocalAddress() && inetAddress.isSiteLocalAddress()) {
		            	ip = inetAddress.getHostAddress().toString();
		            }
		        }
		    }
		}
		catch (Exception ex) {}
		
		List<ActiveUserSession> activeSessions = new ArrayList<ActiveUserSession>();
		for (Object principal : this.sessionRegistry.getAllPrincipals()) {
			for (SessionInformation session : sessionRegistry.getAllSessions(principal, false)) {
				int userKey = -1;
				String userId = "";
				String sitename = "";
				if (principal instanceof User) {
					User cdmsUser = (User) principal;
					userId = cdmsUser.getUsername();
				} else {
					userId = principal.toString();
				}
				
				ActiveUserSession activeUser = null;
				for (ActiveUserSession usi : activeSessions) {
					if (usi.getUserKey() == userKey && usi.getUserId().equals(userId)) {
						activeUser = usi;
						break;
					}
				}
				
				if (activeUser == null) {
					activeUser = new ActiveUserSession();
					activeUser.setUserKey(userKey);
					activeUser.setUserId(userId);
					activeUser.setSitename(sitename);
					activeSessions.add(activeUser);
				}
				activeUser.addSessionInformation(session);
			}
		}
		
		return "system/ehcache-list";
	}
	
	public class ActiveUserSession {

		private int userKey;
		
		private String userId;
		
		private String sitename;
		
		private List<SessionInformation> sessionInformations;
		
		public void addSessionInformation(SessionInformation session) {
			if (this.sessionInformations == null) {
				this.sessionInformations = new ArrayList<SessionInformation>();
			}
			this.sessionInformations.add(session);
		}
		
		@Override
		public boolean equals(Object obj) {
			if (obj instanceof ActiveUserSession) {
				ActiveUserSession compareSession = (ActiveUserSession) obj;
				return (compareSession.getUserKey() == this.userKey && compareSession.getUserId().equals(this.userId));
			}
			return super.equals(obj);
		}

		public int getUserKey() {
			return userKey;
		}

		public void setUserKey(int userKey) {
			this.userKey = userKey;
		}

		public String getUserId() {
			return userId;
		}

		public void setUserId(String userId) {
			this.userId = userId;
		}

		public String getSitename() {
			return sitename;
		}

		public void setSitename(String sitename) {
			this.sitename = sitename;
		}

		public List<SessionInformation> getSessionInformations() {
			return sessionInformations;
		}

		public void setSessionInformations(List<SessionInformation> sessionInformations) {
			this.sessionInformations = sessionInformations;
		}
	}
	
}
