package kr.co.pyj246.webbase.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import kr.co.pyj246.webbase.constant.DefaultConstant;

import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.WebAttributes;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

@Controller
@RequestMapping(value = "/login")
public class AccessController implements MessageSourceAware {
	
    private MessageSource messageSource;
    
    @Override
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
    
    @RequestMapping(value = "")
    public String login(Authentication authentication) {
    	// 인증 성공상태이면 홈화면으로 보낸다.
    	if(authentication != null &&authentication.isAuthenticated()) {
    		return "redirect:/";
    	}
    	return "access.login";
    }
    
    public ModelAndView init(@PathVariable("studyId") final String studyId) {
		RedirectView redirectView = new RedirectView(String.format("/%s/entry/cyclevisit", studyId));
		redirectView.setExposeModelAttributes(false);
		return new ModelAndView(redirectView);
   	}
    
    @RequestMapping(value = "/success")
    public void loginSucess(Model model, 
    		HttpSession httpSession) {
    	model.asMap().clear();
    	String redirectUrl = (String) httpSession.getAttribute(DefaultConstant.AUTHENTICATION_REDIRECT_URL);
    	if(redirectUrl != null) {
    		httpSession.removeAttribute(DefaultConstant.AUTHENTICATION_REDIRECT_URL);
    	}
    	model.addAttribute("result", "success");
    	model.addAttribute("redirectUrl", redirectUrl);
    }
    
    @RequestMapping(value = "/fail")
    public void loginFail(Model model,
    		HttpSession httpSession,
            HttpServletRequest request) {
    	model.asMap().clear();
    	AuthenticationException exception = (AuthenticationException) httpSession.getAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
        if (exception != null) {
            httpSession.removeAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
        }
        model.addAttribute("result", "fail");
    }
    
    @RequestMapping(value = "/loginkeepalive", method = RequestMethod.POST)
    public ResponseEntity<String> sessionKeepAlive() {
        return new ResponseEntity<String>("{\"keepalive\":true}", HttpStatus.OK);
    }
}
