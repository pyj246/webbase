package kr.co.pyj246.webbase.controller;

import org.springframework.beans.factory.annotation.Autowired;

import kr.co.pyj246.webbase.service.EditService;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping(value = "/edit")
public class EditController {
	
	@Autowired
	private EditService editService;
	
	@RequestMapping(value = "/{type:formserialize|ajaxserialize|ajaxjson}")
	public String edit(@PathVariable("type") final String type) {
		return "edit/edit";
	}
	
	@RequestMapping(value = "/formserialize/submit", method = RequestMethod.POST)
	public String formSerializeSubmit(Model model,
			Authentication authentication,
			@RequestParam("editInputText") String editInputText,
			@RequestParam("editInputTextDateTime") String editInputTextDateTime,
			@RequestParam("editInputTextDate") String editInputTextDate,
			@RequestParam("editInputTextTime") String editInputTextTime,
			@RequestParam("editTextarea") String editTextarea,
			@RequestParam("editTextEditor") String editTextEditor,
			@RequestParam("editSelect") String editSelect,
			@RequestParam(value = "editSelectMultiple", required = false) String[] editSelectMultiple,
			@RequestParam(value = "editInputCheck", required = false) String editInputCheck,
			@RequestParam(value = "editInputCheckMultiple", required = false) String[] editInputCheckMultiple,
			@RequestParam(value = "editInputText", required = false) String editInputRadio,
			@RequestParam(value = "editInputFile", required = false) String[] editInputFile) {
		
		System.out.print("editInputText\t → ");
		System.out.println(editInputText);
		System.out.print("editInputTextDateTime\t → ");
		System.out.println(editInputTextDateTime);
		System.out.print("editInputTextDate\t → ");
		System.out.println(editInputTextDate);
		System.out.print("editInputTextTime\t → ");
		System.out.println(editInputTextTime);
		System.out.print("editTextarea\t → ");
		System.out.println(editTextarea);
		System.out.print("editTextEditor\t → ");
		System.out.println(editTextEditor);
		System.out.print("editSelect\t → ");
		System.out.println(editSelect);
		System.out.print("editSelectMultiple\t → ");
		System.out.println(editSelectMultiple);
		System.out.print("editInputCheck\t → ");
		System.out.println(editInputCheck);
		System.out.print("editInputCheckMultiple\t → ");
		System.out.println(editInputCheckMultiple);
		System.out.print("editInputText\t → ");
		System.out.println(editInputRadio);
		System.out.print("editInputFile\t → ");
		System.out.println(editInputFile);
		
		System.out.println("======================================");
		System.out.println("");
		
		return "redirect:/edit/formserialize";
	}
	
	@RequestMapping(value = "/ajaxserialize/submit", method = RequestMethod.POST)
	public void ajaxSerializeSubmit(Model model,
			Authentication authentication,
			@RequestParam("editInputText") String editInputText,
			@RequestParam("editInputTextDateTime") String editInputTextDateTime,
			@RequestParam("editInputTextDate") String editInputTextDate,
			@RequestParam("editInputTextTime") String editInputTextTime,
			@RequestParam("editTextarea") String editTextarea,
			@RequestParam("editTextEditor") String editTextEditor,
			@RequestParam("editSelect") String editSelect,
			@RequestParam("editSelectMultiple") String[] editSelectMultiple,
			@RequestParam("editInputCheck") String editInputCheck,
			@RequestParam("editInputCheckMultiple") String[] editInputCheckMultiple,
			@RequestParam("editInputText") String editInputRadio,
			@RequestParam(value = "editInputFile", required = false) String[] editInputFile) {
		
		System.out.print("editInputText\t → ");
		System.out.println(editInputText);
		System.out.print("editInputTextDateTime\t → ");
		System.out.println(editInputTextDateTime);
		System.out.print("editInputTextDate\t → ");
		System.out.println(editInputTextDate);
		System.out.print("editInputTextTime\t → ");
		System.out.println(editInputTextTime);
		System.out.print("editTextarea\t → ");
		System.out.println(editTextarea);
		System.out.print("editTextEditor\t → ");
		System.out.println(editTextEditor);
		System.out.print("editSelect\t → ");
		System.out.println(editSelect);
		System.out.print("editSelectMultiple\t → ");
		System.out.println(editSelectMultiple);
		System.out.print("editInputCheck\t → ");
		System.out.println(editInputCheck);
		System.out.print("editInputCheckMultiple\t → ");
		System.out.println(editInputCheckMultiple);
		System.out.print("editInputText\t → ");
		System.out.println(editInputRadio);
		System.out.print("editInputFile\t → ");
		System.out.println(editInputFile);
		
		System.out.println("======================================");
		System.out.println("");
		
		model.addAttribute("reload", true);
	}
	
	@RequestMapping(value = "/ajaxjson/submit", method = RequestMethod.POST)
	public void ajaxJsonSubmit(Model model,
			Authentication authentication,
			@RequestParam("dataJson") String dataJson) {
		editService.setEditData(dataJson, authentication.getName());
		model.addAttribute("reload", true);
	}
	
}
