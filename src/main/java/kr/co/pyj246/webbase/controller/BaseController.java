package kr.co.pyj246.webbase.controller;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.co.pyj246.webbase.component.FileManager;
import kr.co.pyj246.webbase.message.DatabaseMessageSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.support.DelegatingMessageSource;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = "/base")
public class BaseController {
	
	@Autowired
	private LocaleResolver localeResolver;
	
	@Autowired
	private MessageSource messageSource;
	
	@Autowired
	private FileManager fileManager;
	
	@RequestMapping(value = "/language", method = RequestMethod.POST)
	public void language(Model model,
			 HttpServletRequest request) {
		model.asMap().clear();
    	model.addAttribute("language", this.localeResolver.resolveLocale(request).getLanguage());
	}
	
	@RequestMapping(value = "/language-set", method = RequestMethod.POST)
	public ModelAndView setLanguage(HttpServletRequest request, HttpServletResponse response,
			@RequestParam("language") String language) {
		this.localeResolver.setLocale(request, response, new Locale(language));
		return new ModelAndView();
	}
	
	@RequestMapping(value = "/message", method = RequestMethod.POST)
	public void messageData(Model model,
			@RequestParam("language") String language) {
		model.asMap().clear();
		model.addAttribute("messageData", ((DatabaseMessageSource) ((DelegatingMessageSource) messageSource).getParentMessageSource()).getAllMessages(language));
	}
	
	@RequestMapping(value = "/fileUpload", method = RequestMethod.POST)
	public void upload(Model model, 
			HttpServletRequest request,
			Authentication authentication,
			@RequestParam("uploadFile") MultipartFile uploadFile,
			@RequestParam("index") int index) {
		System.out.println(index);
		System.out.println(uploadFile.getOriginalFilename());
		System.out.println("-----------");
		model.addAttribute("index", index);
		model.addAttribute("fileName", uploadFile.getOriginalFilename());
		model.addAttribute("fileKey", index);
	}
	
	@RequestMapping(value = "/fileDownload", method = RequestMethod.POST)
	public void fileDownload(Model model,
			@RequestParam("fileDownloadKey") int fileDownloadKey) {
		model.asMap().clear();
		model.addAttribute("fileDownload", fileManager.getFileDownload(fileDownloadKey));
	}
}
