package kr.co.pyj246.webbase.controller;

import kr.co.pyj246.webbase.component.CsvManager;
import kr.co.pyj246.webbase.component.ExcelManager;
import kr.co.pyj246.webbase.component.FileManager;
import kr.co.pyj246.webbase.component.PdfManager;
import kr.co.pyj246.webbase.constant.DefaultConstant;
import kr.co.pyj246.webbase.service.DownloadService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/download")
public class DownloadController {
	
	@Autowired
	private DownloadService downloadService;
	
	@Autowired
	private FileManager fileManager;
	
	@Autowired
	private CsvManager csvManager;
	
	@Autowired
	private ExcelManager excelManager;
	
	@Autowired
	private PdfManager pdfManager;
	
	@RequestMapping(value = "/csv")
	public String downloadMakeCsv(Model model,
			Authentication authentication) {
		
		int fileKey = csvManager.writeFile("testCSV", downloadService.getTestCsvContents(), 1);
		
		model.addAttribute(DefaultConstant.DOWNLOAD_FILE_KEY, fileKey);
		model.addAttribute(DefaultConstant.DOWNLOAD_USER_KEY, 1);
		
		return "default-file-export";
	}
	
	@RequestMapping(value = "/direct/csv")
	public String downloadDirectCsv(Model model,
			Authentication authentication) {
		
		model.addAttribute(DefaultConstant.DOWNLOAD_FILE_NAME, "testCSV");
		model.addAttribute(DefaultConstant.DOWNLOAD_FILE_CONTENTS, downloadService.getTestCsvContents());
		
		return "default-csv-export";
	}
	
	@RequestMapping(value = "/excel")
	public String downloadMakeExcel(Model model,
			Authentication authentication) {
		
		int fileKey = excelManager.writeFile("testExcel", downloadService.getTestWorkbook(), 1);
		
		model.addAttribute(DefaultConstant.DOWNLOAD_FILE_KEY, fileKey);
		model.addAttribute(DefaultConstant.DOWNLOAD_USER_KEY, 1);
		
		return "default-file-export";
	}
	
	@RequestMapping(value = "/direct/excel")
	public String downloadDirectExcel(Model model,
			Authentication authentication) {
		
		model.addAttribute(DefaultConstant.DOWNLOAD_FILE_NAME, "testExcel");
		model.addAttribute(DefaultConstant.DOWNLOAD_FILE_CONTENTS, downloadService.getTestWorkbook());
		
		return "default-excel-export";
	}
	
	@RequestMapping(value = "/pdf")
	public String downloadMakePdf(Model model,
			Authentication authentication) {
		
		int fileKey = pdfManager.writeFile("testPdf", downloadService.getTestPdfHtml(), false, 1);
				
		model.addAttribute(DefaultConstant.DOWNLOAD_FILE_KEY, fileKey);
		model.addAttribute(DefaultConstant.DOWNLOAD_USER_KEY, 1);
		
		return "default-file-export";
	}

	@RequestMapping(value = "/pdfa")
	public String downloadMakePdfa(Model model,
			Authentication authentication) {
		
		int fileKey = pdfManager.writeFile("testPdf", downloadService.getTestPdfHtml(), true, 1);
		
		model.addAttribute(DefaultConstant.DOWNLOAD_FILE_KEY, fileKey);
		model.addAttribute(DefaultConstant.DOWNLOAD_USER_KEY, 1);
		
		return "default-file-export";
	}
	
	@RequestMapping(value = "/direct/pdf")
	public String downloadDirectPdf(Model model,
			Authentication authentication) {
		
		model.addAttribute(DefaultConstant.DOWNLOAD_FILE_NAME, "testPdf");
		model.addAttribute(DefaultConstant.DOWNLOAD_FILE_CONTENTS, downloadService.getTestPdfHtml());
		
		return "default-pdf-export";
	}
	
	@RequestMapping(value = "/direct/pdfa")
	public String downloadDirectPdfa(Model model,
			Authentication authentication) {
		
		model.addAttribute(DefaultConstant.DOWNLOAD_FILE_NAME, "testPdfa");
		model.addAttribute(DefaultConstant.DOWNLOAD_FILE_CONTENTS, downloadService.getTestPdfHtml());
		model.addAttribute(DefaultConstant.DOWNLOAD_PDF_IS_ARCHIVE, true);
		
		return "default-pdf-export";
	}
	
	@RequestMapping(value = "/file")
	public String downloadFile(Model model,
			Authentication authentication) {
		
		model.addAttribute(DefaultConstant.DOWNLOAD_FILE_KEY, 1);
		model.addAttribute(DefaultConstant.DOWNLOAD_USER_KEY, 1);
		
		return "default-file-export";
	}
}
