package kr.co.pyj246.webbase.controller;

import java.net.SocketException;
import java.util.Enumeration;
import java.util.Map;

import javax.net.ssl.SSLException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import kr.co.pyj246.webbase.constant.DefaultConstant;
import kr.co.pyj246.webbase.exception.DefinedException;
import kr.co.pyj246.webbase.exception.MessageException;
import kr.co.pyj246.webbase.model.user.UserDetail;

import org.apache.log4j.lf5.LogLevel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.view.RedirectView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

@Controller
@ControllerAdvice
public class ExceptionController {
	private static final Logger LOGGER = LoggerFactory.getLogger(ExceptionController.class);

    private static final String EXCEPTION_VIEW_NAME = "error-default";

    /**
     * 오류 발생시 기본 처리
     * .
     * @param request
     * @param response
     * @param authentication
     * @param exception
     * @return
     */
    @ExceptionHandler()
    public ModelAndView defaultExceptionHandler(
            HttpServletRequest request,
            HttpServletResponse response,
            Authentication authentication,
            Exception exception) {
        /** Socket Exception 의 경우에는 단순 정보 로그만 남도록 수정. */
        if (exception instanceof SocketException
                || exception.getCause() instanceof SocketException
                || exception.getCause() instanceof SSLException) {
            LOGGER.info("Socket Exception", exception.getMessage());
            ModelAndView view = new ModelAndView();
            view.setView(new MappingJackson2JsonView());
            return view;
        }

        /**
         * Java Agent 에 의한 Connection 확인 Request 일 경우에 대한 처리.
         */
        if (request.getHeader("User-Agent").matches("Java/[0-9_.]+") && exception instanceof HttpRequestMethodNotSupportedException) {
            ModelAndView view = new ModelAndView();
            view.setView(new MappingJackson2JsonView());
            return view;
        }

        writeExceptionLoggerMessage(request, authentication, null, exception);

        return getModelAndView(request, response, exception);
    }
    
    @ExceptionHandler(MessageException.class)
    public ModelAndView messageDeliveryExceptionHandler(
    		HttpServletRequest request,
            HttpServletResponse response,
            Authentication authentication,
            Exception exception) {
    	writeExceptionLoggerMessage(request, authentication, null, exception, null, LogLevel.INFO);
    	return getModelAndView(request, response, exception);
    }

    private void writeExceptionLoggerMessage(HttpServletRequest request, Authentication authentication,
            String requestBody, Exception exception) {
        writeExceptionLoggerMessage(request, authentication, requestBody, exception, null, LogLevel.ERROR);
    }

    private void writeExceptionLoggerMessage(HttpServletRequest request, Authentication authentication,
            String requestBody, Exception exception, String customMessage, LogLevel level) {
        HttpSession httpSession = request.getSession(false);
        UserDetail user = null;
        if (httpSession != null) {
            Object userDetails = httpSession.getAttribute(DefaultConstant.SESSION_USER);
            if (userDetails instanceof UserDetail) {
                user = (UserDetail) userDetails;
            }
        }
        StringBuilder message = new StringBuilder();
        message.append("\n\nRequestUrl: ").append(request.getRequestURL());
        if (user != null) {
            message.append("\n\nUserKey: ").append(user.getKey())
                    .append("\n\nUserId: ").append(user.getEmail());
        }
        message.append("\n\nReferer: ").append(request.getHeader("Referer"))
                .append("\n\nUser-Agent: ").append(request.getHeader("User-Agent"))
                .append("\n\nParameters: ")
                .append("\n");

        if (request != null) {
            message.append("\n*************************************************************")
                    .append("\nREQUEST")
                    .append("\n*************************************************************\n");
            Enumeration<String> headerNames = request.getHeaderNames();
            if (headerNames != null) {
                while (headerNames.hasMoreElements()) {
                    String headerName = headerNames.nextElement();
                    message.append("\n").append(headerName.toUpperCase())
                            .append(": \"").append(request.getHeader(headerName)).append("\"");
                }
            }

            if (requestBody != null) {
                message.append("\n\nMethod: \"").append(request.getMethod().toUpperCase()).append("\"")
                        .append("\nRequestBody: \"").append(requestBody).append("\"");
            } else {
                Map<String, String[]> parameterMap = request.getParameterMap();
                if (parameterMap != null && parameterMap.size() > 0) {
                    message.append("\n\nMethod: ").append(request.getMethod().toUpperCase())
                            .append("\nParameters:");
                    for (String key : parameterMap.keySet()) {
                        message.append("\n[ ").append(key).append(" ] => [ ");
                        boolean isFirst = true;
                        for (String value : parameterMap.get(key)) {
                            if (!isFirst) {
                                message.append(" , ");
                            }
                            message.append(value);
                            isFirst = false;
                        }
                        message.append(" ]");
                    }
                }
            }
        }

        message.append("\n");

        message.append("\n*************************************************************")
                .append("\nEXCEPTION DETAIL")
                .append("\n*************************************************************\n\n");
        
        if(LogLevel.DEBUG.equals(level)) {
        	if (exception == null) {
                LOGGER.debug(message.toString());
            } else {
                LOGGER.debug(message.toString(), exception);
            }
        } else if(LogLevel.INFO.equals(level)) {
        	if (exception == null) {
                LOGGER.info(message.toString());
            } else {
                LOGGER.info(message.toString(), exception);
            }
        } else if(LogLevel.WARN.equals(level)) {
        	if (exception == null) {
                LOGGER.warn(message.toString());
            } else {
                LOGGER.warn(message.toString(), exception);
            }
        } else {
        	if (exception == null) {
                LOGGER.error(message.toString());
            } else {
                LOGGER.error(message.toString(), exception);
            }
        }
    }

    private ModelAndView getModelAndView(HttpServletRequest request, HttpServletResponse response, Exception exception) {
        ModelAndView modelAndView = new ModelAndView();
        if (request != null && request.getHeader("Accept") != null && request.getHeader("Accept").contains("json")) {
            modelAndView.setView(new MappingJackson2JsonView());
            modelAndView.addObject("exception", true);
            if(exception instanceof MessageException) {
            	modelAndView.addObject("exception", exception.getMessage());
        		modelAndView.addObject("errorCode", ((MessageException) exception).getErrorCode());
            } else if(exception instanceof DefinedException) {
                modelAndView.addObject("exception", exception.getMessage());
            } else {
                modelAndView.addObject("exception", "");
            }
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
        } else if(exception instanceof NoHandlerFoundException) {
        	RedirectView redirectView = new RedirectView("/");
    		redirectView.setExposeModelAttributes(false);
    		modelAndView = new ModelAndView(redirectView);
        } else {
        	 modelAndView.setViewName(EXCEPTION_VIEW_NAME);
             String refererURI = request != null ? request.getHeader("Referer") : "";
             if(refererURI != null && refererURI.endsWith("login")) {
                 refererURI = "/";
             }
         	modelAndView.addObject("forwardURI", refererURI);
             modelAndView.addObject("message", exception.getMessage());
        }
        
        return modelAndView;
    }
}
