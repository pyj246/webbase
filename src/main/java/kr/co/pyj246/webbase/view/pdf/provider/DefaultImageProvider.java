package kr.co.pyj246.webbase.view.pdf.provider;

import com.itextpdf.tool.xml.pipeline.html.AbstractImageProvider;
import org.springframework.web.context.ServletContextAware;

import javax.servlet.ServletContext;

public class DefaultImageProvider extends AbstractImageProvider implements ServletContextAware {

    private String rootPath;

    @Override
    public String getImageRootPath() {
        return rootPath;
    }

    @Override
    public void setServletContext(ServletContext servletContext) {
        rootPath = servletContext.getRealPath("/");
    }
}
