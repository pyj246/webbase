package kr.co.pyj246.webbase.view.csv;

import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.co.pyj246.webbase.component.CsvManager;
import kr.co.pyj246.webbase.constant.DefaultConstant;
import kr.co.pyj246.webbase.view.AbstractDownloadView;

import org.springframework.beans.factory.annotation.Autowired;

public class DefaultCsvView extends AbstractDownloadView {
	
	@Autowired
	private CsvManager csvManager;
	
	@SuppressWarnings("unchecked")
	@Override
	protected void processDownload(ServletOutputStream servletOutputStream,
			Map<String, Object> model, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		List<List<String>> csvContents = null;
		String downloadCookieResult = "fail";
		
		try {
			csvContents = (List<List<String>>) model.get(DefaultConstant.DOWNLOAD_FILE_CONTENTS);
			
			// Set File Cookie (Success)
			downloadCookieResult = "success";
		} finally {
			
			// Set Download Result Cookie
			Cookie downloadCompleteCookie = new Cookie(DefaultConstant.DOWNLOAD_COMPLETE_COOKIE, downloadCookieResult);
			downloadCompleteCookie.setPath("/");
			response.addCookie(downloadCompleteCookie);
			
			if(csvContents != null) {
				// Set Filename
				if(model.containsKey(DefaultConstant.DOWNLOAD_FILE_NAME)) {
					setDownloadFilename(request, response, 
							model.get(DefaultConstant.DOWNLOAD_FILE_NAME).toString(), "csv");
		        }
				
				csvManager.writeOutputStream(servletOutputStream, csvContents);
			}
		}
	}
	
	@Override
	protected String getDownloadContentType() {
		return csvManager.getContentType();
	}
}
