package kr.co.pyj246.webbase.view;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.view.AbstractView;

public abstract class AbstractDownloadView extends AbstractView {
	
	@Override
	protected void renderMergedOutputModel(Map<String, Object> model,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
		ServletOutputStream servletOutputStream = null;
		
		try {
			// Set Content Type
			response.setContentType(getDownloadContentType());
			
			// Render Output Model
			servletOutputStream = response.getOutputStream(); 
			
			// Process Download 
			processDownload(servletOutputStream, model, request, response);
			
		} finally {
			// flush ServletOutputStream
			if(servletOutputStream != null) {
				servletOutputStream.flush();
			}
		}
	}
	
	protected abstract void processDownload(ServletOutputStream servletOutputStream, Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception;
	
	protected abstract String getDownloadContentType();
	
	protected void setDownloadFilename(HttpServletRequest request, HttpServletResponse response, String filename, String fileExtension) throws UnsupportedEncodingException {
		String contentDisposition = null;
		if(fileExtension == null){
			contentDisposition = String.format("attachment;filename=\"%s\";", filenameAsToByBrowserVendor(request.getHeader("User-Agent").toString(), filename));
		} else {
			contentDisposition = String.format("attachment;filename=\"%s.%s\";", filenameAsToByBrowserVendor(request.getHeader("User-Agent"), filename).toString(), fileExtension);
		}
		response.setHeader("Content-Transfer-Encoding", "binary");
    	response.setHeader("Content-Disposition", contentDisposition);
	}
	
	protected String filenameAsToByBrowserVendor(String userAgent, String filename) throws UnsupportedEncodingException {
        if (userAgent.contains("MSIE")) {
            return URLEncoder.encode(filename, "UTF-8").replaceAll("\\+", "%20");
        }
        return new String(filename.getBytes("UTF-8"), "ISO-8859-1");
    }
}
