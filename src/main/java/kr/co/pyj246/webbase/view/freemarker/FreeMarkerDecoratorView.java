package kr.co.pyj246.webbase.view.freemarker;

import org.springframework.web.servlet.view.freemarker.FreeMarkerView;

import java.io.FileNotFoundException;
import java.util.Locale;

public class FreeMarkerDecoratorView extends FreeMarkerView {

    private String contentPath;

    public void setContentPath(String contentPath) {
        this.contentPath = contentPath;
    }

    @Override
    public boolean checkResource(Locale locale) throws Exception {
        if (contentPath != null) {
            try {
                getTemplate(contentPath, locale);
            } catch (FileNotFoundException e) {
                if (logger.isDebugEnabled()) {
                    logger.debug("No FreeMarker content view found for URL: " + contentPath);
                }
                return false;
            }
        }

        return super.checkResource(locale);
    }
}
