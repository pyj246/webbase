package kr.co.pyj246.webbase.view.pdf.provider;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.FontProvider;
import com.itextpdf.text.pdf.BaseFont;

public class DefaultFontProvider implements FontProvider {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultFontProvider.class);
	
	private Map<String, BaseFont> fonts;
	
	public DefaultFontProvider() {
		super();
		 Map<String, String> fontPaths = new HashMap<String, String>(5);
         fontPaths.put("NanumGothic", "kr/co/pyj246/webbase/view/pdf/fonts/NanumGothic.ttf");
         fontPaths.put("NanumGothicBold", "kr/co/pyj246/webbase/view/pdf/fonts/NanumGothicBold.ttf");
         fontPaths.put("NanumGothicExtraBold", "kr/co/pyj246/webbase/view/pdf/fonts/NanumGothicExtraBold.ttf");
         fontPaths.put("NanumMyeongjo", "kr/co/pyj246/webbase/view/pdf/fonts/NanumMyeongjo.ttf");
         fontPaths.put("NanumMyeongjoBold", "kr/co/pyj246/webbase/view/pdf/fonts/NanumMyeongjoBold.ttf");
         fontPaths.put("NanumMyeongjoExtraBold", "kr/co/pyj246/webbase/view/pdf/fonts/NanumMyeongjoExtraBold.ttf");
         fontPaths.put("DejaVuSansBold", "kr/co/pyj246/webbase/view/pdf/fonts/DejaVuSans-Bold.ttf");
         setFonts(fontPaths);
	}
	
    public void setFonts(Map<String, String> fontPaths) {
		fonts = new HashMap<String, BaseFont>(fontPaths.size());
        for (String name : fontPaths.keySet()) {
            String path = fontPaths.get(name);
            try {
                BaseFont font = BaseFont.createFont(path, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
                fonts.put(name, font);
            } catch (Exception e) {
                LOGGER.warn("Fail to load font: " + name + ", path: " + path, e);
            }
        }
    }

    @Override
    public boolean isRegistered(String fontName) {
        boolean registered = false;
        for (String name : fonts.keySet()) {
            if (name.equalsIgnoreCase(fontName)) {
                registered = true;
                break;
            }
        }
        if (!registered) {
            registered = FontFactory.isRegistered(fontName);
        }
        return registered;
    }

    @Override
    public Font getFont(String fontName, String encoding, boolean embedded, float size, int style, BaseColor baseColor) {
        Font font = null;
    	for (String name : fonts.keySet()) {
            if (name.equalsIgnoreCase(fontName)) {
                font = new Font(fonts.get(name), size, style, baseColor);
                break;
            }
        }
        if (font == null) {
            font = new Font(fonts.get("NanumGothic"), size, style, baseColor);
        }
        return font;
    }
	
}