package kr.co.pyj246.webbase.view.freemarker;

import org.springframework.web.servlet.view.AbstractUrlBasedView;
import org.springframework.web.servlet.view.freemarker.FreeMarkerViewResolver;

/**
 * FreeMarker View 에 Decorator pattern 을 적용하기 위한 View Resolver.
 * 
 * @file FreeMarkerDecoratorViewResolver.java 
 * @time 오후 3:04:51
 * @author yjpark
 */
public class FreeMarkerDecoratorViewResolver extends FreeMarkerViewResolver {

    public static final String KEY_CONTENT_PATH = "contentPath";

    public static final String HTML_PREFIX = "html:";

    private String decoratorTemplate;
    
    public static String getHtmlViewName(String viewName) {
    	return String.format("%s%s", HTML_PREFIX, viewName);
    }
    
    public void setDecoratorTemplateName(String decoratorTemplateName) {
        decoratorTemplate = getPrefix() + decoratorTemplateName + getSuffix();
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
    protected Class requiredViewClass() {
        return FreeMarkerDecoratorView.class;
    }

    @Override
    protected AbstractUrlBasedView buildView(String viewName) throws Exception {
        AbstractUrlBasedView view = super.buildView(viewName);
        
        // View 처리 분기
        if (viewName.startsWith(HTML_PREFIX)) {
        	if (view instanceof FreeMarkerDecoratorView) {
                ((FreeMarkerDecoratorView) view).setContentPath(getPrefix() + normalizeContentPath(viewName.substring(HTML_PREFIX.length())) + getSuffix());
            }
        	
			view.setUrl(getPrefix() + viewName.substring(HTML_PREFIX.length()) + getSuffix());
		} else {
			if (view instanceof FreeMarkerDecoratorView) {
	            ((FreeMarkerDecoratorView) view).setContentPath(getPrefix() + normalizeContentPath(viewName) + getSuffix());
	        }
			
			view.setUrl(decoratorTemplate);
	        view.addStaticAttribute(KEY_CONTENT_PATH, viewName + getSuffix());
		}
        
        return view;
    }

    private String normalizeContentPath(String path) {
        return path.replace('.', '/');
    }
}
