package kr.co.pyj246.webbase.view.pdf;

import java.io.StringReader;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;

import kr.co.pyj246.webbase.component.PdfManager;
import kr.co.pyj246.webbase.constant.DefaultConstant;
import kr.co.pyj246.webbase.view.AbstractDownloadView;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.parser.XMLParser;

public class DefaultPdfView extends AbstractDownloadView {
	
	@Autowired
	private PdfManager pdfManager;
	
	@Override
	protected void processDownload(ServletOutputStream servletOutputStream,
			Map<String, Object> model, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		Document document = null;
		PdfWriter pdfWriter = null;
		String downloadCookieResult = "fail";
		
		try {
			document = new Document();
			
			// Get PdfWriter
			boolean isArchive = false;
			if(model.containsKey(DefaultConstant.DOWNLOAD_PDF_IS_ARCHIVE)) {
				if(model.get(DefaultConstant.DOWNLOAD_PDF_IS_ARCHIVE) instanceof Boolean) {
					isArchive = (boolean) model.get(DefaultConstant.DOWNLOAD_PDF_IS_ARCHIVE);
				}
			}
			pdfWriter = pdfManager.getPdfWriter(document, servletOutputStream, isArchive);
			
			// Open Document
			document.open();
			
			if(isArchive) {
				pdfManager.setIccProfile(pdfWriter);
			}
			pdfWriter.setViewerPreferences(PdfWriter.ALLOW_PRINTING | PdfWriter.PageLayoutSinglePage);
			pdfWriter.setViewerPreferences(PdfWriter.PageModeUseOutlines);
			
			String contentHTML = "&nbsp;";
			if(model.containsKey(DefaultConstant.DOWNLOAD_FILE_CONTENTS) && model.get(DefaultConstant.DOWNLOAD_FILE_CONTENTS) instanceof String) {
				contentHTML = (String) model.get(DefaultConstant.DOWNLOAD_FILE_CONTENTS);
				XMLParser parser = pdfManager.getXMLParser(document, pdfWriter);
		        parser.parse(new StringReader(contentHTML));
			} else {
				document.add(new Paragraph(" ", pdfManager.getDefaultFontProvider().getFont("NanumGothic", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 10, -1, BaseColor.WHITE)));
			}
			
			// Set File Cookie (Success)
			downloadCookieResult = "success";
		} finally {
			// Set Filename
			if(model.containsKey(DefaultConstant.DOWNLOAD_FILE_NAME)) {
				setDownloadFilename(request, response, 
						model.get(DefaultConstant.DOWNLOAD_FILE_NAME).toString(), "pdf");
	        }
			
			// Set Download Result Cookie
			Cookie downloadCompleteCookie = new Cookie(DefaultConstant.DOWNLOAD_COMPLETE_COOKIE, downloadCookieResult);
			downloadCompleteCookie.setPath("/");
			response.addCookie(downloadCompleteCookie);
			
			// Close Document
			if(document != null) {
				document.close();
			}
			
			// Close PdfWriter
		    if(pdfWriter != null) {
				try {
					pdfWriter.close();
				} catch(Exception e){
					pdfWriter = null;
				}
		    }
		}
	}
	
	@Override
	protected String getDownloadContentType() {
		return pdfManager.getContentType();
	}
}
