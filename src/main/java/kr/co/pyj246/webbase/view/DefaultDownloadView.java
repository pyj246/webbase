package kr.co.pyj246.webbase.view;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;

import kr.co.pyj246.webbase.component.FileManager;
import kr.co.pyj246.webbase.constant.DefaultConstant;
import kr.co.pyj246.webbase.model.base.DataState;
import kr.co.pyj246.webbase.model.file.FileInfo;


public class DefaultDownloadView extends AbstractDownloadView {
	
	private final int BUFFER_SIZE = 8192;
	
	private final long FLUSH_THRESHOLD = 1024*1024;
	
	@Autowired
	private FileManager fileManager;
	
	@Override
	protected void processDownload(ServletOutputStream servletOutputStream,
			Map<String, Object> model, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		int fileKey = (int) model.get(DefaultConstant.DOWNLOAD_FILE_KEY);
		int userKey = (int) model.get(DefaultConstant.DOWNLOAD_USER_KEY);
		
		FileInfo fileInfo = fileManager.getFileInfo(fileKey);
		
		// Set Filename
		if(fileInfo.getName() != null && fileInfo.getExtension() != null) {
			setDownloadFilename(request, response, 
					fileInfo.getName(), fileInfo.getExtension());
        }
		
		// Set Content Type
		super.setContentType(fileInfo.getContentType());
		
		// Insert File Download
		int fileDownloadKey = fileManager.insertFileDownload(fileKey, userKey);
		
		// Write Response
		FileInputStream fileInputStream = null;
		long fileSize = 0, flushSize = 0;
		DataState downloadState = DataState.Fail;
		
		try {
			File file = new File(fileInfo.getPath());
		    fileInputStream  = new FileInputStream(file);
		    
		    fileSize = file.length();
		    
		    // Set Download Result Cookie
		    String fileCookie = String.format("%d::%d", fileDownloadKey, fileSize);
    		Cookie downloadCompleteCookie = new Cookie(DefaultConstant.DOWNLOAD_COMPLETE_COOKIE, fileCookie);
    		downloadCompleteCookie.setPath("/");
    		response.addCookie(downloadCompleteCookie);
		    
    		
    		// File Flush
		    byte[] byteBuffer =new byte[BUFFER_SIZE];
		    int bytesread = 0, bytesBuffered = 0;
		    while((bytesread = fileInputStream.read(byteBuffer)) > -1 ) {
		    	servletOutputStream.write(byteBuffer, 0, bytesread);
		        bytesBuffered += bytesread;
		        if (bytesBuffered > FLUSH_THRESHOLD) { //flush after FLUSH_THRESHOLD
		        	flushSize += bytesBuffered;
		            bytesBuffered = 0;
		            servletOutputStream.flush();
		            fileManager.updateFileDownloadSize(fileDownloadKey, flushSize, userKey);		
		        }
		    }
		    
		    // Set Success
		    downloadState = DataState.Success;
		} catch(IOException e) {
			// Catch User Abort
			String simpleName = e.getClass().getSimpleName();
			if (simpleName.equals("ClientAbortException")) {
			    downloadState = DataState.Cancel;
			} else {
				downloadState = DataState.Fail;
			    throw e;
			}
		} finally {
			// Close ByteArrayOutputStream
		    if(fileInputStream != null) {
				try {
					fileInputStream.close();
				} catch(Exception e){
					fileInputStream = null;
				}
		    }
		    
			// Set Fiel Download Result
		    fileManager.updateFileDownloadSize(fileDownloadKey, flushSize, userKey);
		    fileManager.updateFileDownloadStatus(fileDownloadKey, downloadState, userKey);
		}
	}
	
	@Override
	protected String getDownloadContentType() {
		return "appliation/x-msdownload";
	}
}
