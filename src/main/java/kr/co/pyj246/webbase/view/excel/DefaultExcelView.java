package kr.co.pyj246.webbase.view.excel;

import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.co.pyj246.webbase.component.ExcelManager;
import kr.co.pyj246.webbase.constant.DefaultConstant;
import kr.co.pyj246.webbase.view.AbstractDownloadView;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;

public class DefaultExcelView extends AbstractDownloadView {
	
	@Autowired
	protected ExcelManager excelManager;
	
	@Override
	protected void processDownload(ServletOutputStream servletOutputStream,
			Map<String, Object> model, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		Workbook workbook = null;
		String downloadCookieResult = "fail";
		
		try {
			// Build excel document.
			workbook = (Workbook) model.get(DefaultConstant.DOWNLOAD_FILE_CONTENTS);
			
			workbook.write(servletOutputStream);
			
			// Set File Cookie (Success)
			downloadCookieResult = "success";
		} finally {
			// Set Download Result Cookie
			Cookie downloadCompleteCookie = new Cookie(DefaultConstant.DOWNLOAD_COMPLETE_COOKIE, downloadCookieResult);
			downloadCompleteCookie.setPath("/");
			response.addCookie(downloadCompleteCookie);
			
			// Set Filename
			if(workbook != null && model.containsKey(DefaultConstant.DOWNLOAD_FILE_NAME)) {
				setDownloadFilename(request, response, 
						model.get(DefaultConstant.DOWNLOAD_FILE_NAME).toString(), 
						excelManager.getFileExtension(workbook));
	        }
		}
	}
	
	@Override
	protected String getDownloadContentType() {
		return excelManager.getContentType();
	}
}