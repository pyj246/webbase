package kr.co.pyj246.webbase.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import kr.co.pyj246.webbase.constant.DefaultConstant;
import kr.co.pyj246.webbase.model.user.UserDetail;

import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class CommonInterceptor extends HandlerInterceptorAdapter implements MessageSourceAware {
	
	private MessageSource messageSource;
    
    public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
	
	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
		return super.preHandle(request, response, handler);
	}
	
	@Override
	public void postHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		String uri = request.getRequestURI();
		HttpSession httpSession = request.getSession(false);
        if (modelAndView != null && httpSession != null) {
        	Object userObject = httpSession.getAttribute(DefaultConstant.SESSION_USER);
            if (userObject != null && userObject instanceof UserDetail) {
            	UserDetail user = (UserDetail) userObject;
            	
            	// 유저정보 주입
            	modelAndView.addObject("userInfo", user);
            }
        }
		super.postHandle(request, response, handler, modelAndView);
	}
}

