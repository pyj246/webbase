package kr.co.pyj246.webbase.component;

import org.apache.poi.ss.usermodel.Workbook;

import java.io.File;

import kr.co.pyj246.webbase.model.base.excel.PoiType;

public interface ExcelManager {
	
	public String getContentType();
	
	public Workbook readFile(File file); 
	
	public Workbook createWorkbook(PoiType poiType);
	
	public Workbook createWorkbook(int rowSize);
	
	public String getFileExtension(Workbook workbook);
	
	public int writeFile(String filename, Workbook workbook, int userKey);
	
}