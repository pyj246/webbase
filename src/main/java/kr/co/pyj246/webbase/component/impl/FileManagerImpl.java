package kr.co.pyj246.webbase.component.impl;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import kr.co.pyj246.webbase.component.FileManager;
import kr.co.pyj246.webbase.model.base.DataState;
import kr.co.pyj246.webbase.model.file.FileDownload;
import kr.co.pyj246.webbase.model.file.FileInfo;
import kr.co.pyj246.webbase.model.file.FileUploadType;
import kr.co.pyj246.webbase.persistence.FileMapper;

@Component
public class FileManagerImpl implements FileManager {
	
	private final String BASE_FILE_STORAGE_PATH = "files";
	
	@Autowired
	private ServletContext servletContext;
	
	@Autowired
	private FileMapper fileMapper;
	
	@Override
	public String normalizeFilePath(String path) {
		return path.replace("\\\\", "/").replace("\\", "/");
	}
	
	@Override
	public String getBaseFileStoragePath() {
		String realPath = normalizeFilePath(servletContext.getRealPath("/"));
		if(realPath.endsWith("/")) {
			realPath = realPath.substring(0, realPath.length() - 1);
		}
		return String.format("%s/%s/", realPath, BASE_FILE_STORAGE_PATH);
	}
	
	@Override
	public String getFileStroagePath(int userKey) {
		return getFileStroagePath(null, userKey);
	}
	
	@Override
	public String getFileStroagePath(String path, int userKey) {
		String basePath = getBaseFileStoragePath();
		if(basePath.endsWith("/")) {
			basePath = basePath.substring(0, basePath.length() - 1);
		}
		if(path != null && !path.isEmpty()) {
			path = normalizeFilePath(path);
			if(path.startsWith("/")) {
				path = path.substring(1, path.length());
			}
			return String.format("%s/%d/%s/", basePath, userKey, path);
		} else {
			return String.format("%s/%d/", basePath, userKey);  
		}
	}
	
	@Override
	public String getTemporaryFilename(String filename, String extension) {
		if(extension != null && !extension.isEmpty()) {
			filename = String.format("%s.%s", filename, extension);
		}
		return String.format("%s_%s", filename, new SimpleDateFormat("yyyy-mm-dd_hh-mm-ss").format(new Date(System.currentTimeMillis())));
	}
	
	@Override
	public File newJavaFile(String filename, int userKey) {
		return newJavaFile(filename, null, userKey);
	}
	
	@Override
	public File newJavaFile(String filename, String path, int userKey) {
		String realPath = getFileStroagePath(path, userKey);
		File directory = new File(realPath);
		if(!(directory.exists())) {
			if(!directory.mkdirs()) {
				throw new RuntimeException("");
			}
		}
		return new File(String.format("%s/%s", realPath, filename));
	}
	
	@Override
	public File readJavaFile(String path) {
		File file = new File(path);
		if(file.exists()) {
			return file;
		} else {
			return null;
		}
	}
	
	@Override
	public FileInfo getFileInfo(int fileKey) {
		return fileMapper.selectFile(fileKey);
	}
	
	@Override
	public int insertFileInfo(String name, String extension, String path, String contentType, FileUploadType uploadType, long size, int userKey) {
		FileInfo fileInfo = new FileInfo();
		fileInfo.setName(name);
		fileInfo.setExtension(extension);
		fileInfo.setPath(path);
		fileInfo.setContentType(contentType);
		fileInfo.setUploadType(uploadType);
		fileInfo.setSize(size);
		fileInfo.setUserKey(userKey);
		fileMapper.insertFile(fileInfo);
		return fileInfo.getKey();
	}
	
	@Override
	public void updateFileInfoStatus(int key, DataState state, int userKey) {
		fileMapper.updateFileStatus(key, state, userKey);
	}
	
	@Override
	public FileDownload getFileDownload(int fileDownloadKey) {
		return fileMapper.selectFileDownload(fileDownloadKey);
	}
	
	@Override
	public int insertFileDownload(int fileKey, int userKey) {
		FileDownload fileDownload = new FileDownload();
		fileDownload.setFileKey(fileKey);
		fileDownload.setUserKey(userKey);
		fileMapper.insertFileDownload(fileDownload);
		return fileDownload.getKey();
	}
	
	@Override
	public void updateFileDownloadSize(int key, long size, int userKey) {
		fileMapper.updateFileDownloadSize(key, size, userKey);
	}
	
	@Override
	public void updateFileDownloadStatus(int key, DataState state, int userKey) {
		fileMapper.updateFileDownloadStatus(key, state, userKey);
	}
}
