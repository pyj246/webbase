package kr.co.pyj246.webbase.component;

import java.util.Map;

public interface ConvertManager {
	
	/**
	 * 오브젝트를 받아 Int형으로 변환하는 메소드
	 * 
	 * @param object
	 * @return
	 */
	int convertObjectIntoInt(Object object);
	
	/**
	 * 오브젝트를 받아 Boolean형으로 변환하는 메소드
	 * 
	 * @param object
	 * @return
	 */
	boolean convertObjectIntoBoolean(Object object);
	
	/**
	 * 오브젝트를 받아 Json으로 변환하는 메소드
	 * 
	 * @param object
	 * @return
	 */
	String convertObjectIntoJson(Object object);
	
	/**
	 * Json String을 받아 Map<String, Object>형으로 변환하는 메소드
	 * 
	 * @param jsonString
	 * @return
	 */
	Map<String, Object> convertJsonStringIntoMap(String jsonString);
	
}
