package kr.co.pyj246.webbase.component;

import java.io.File;

import kr.co.pyj246.webbase.model.base.DataState;
import kr.co.pyj246.webbase.model.file.FileDownload;
import kr.co.pyj246.webbase.model.file.FileInfo;
import kr.co.pyj246.webbase.model.file.FileUploadType;


public interface FileManager {

	public String normalizeFilePath(String path);
	
	public String getBaseFileStoragePath();
	
	public String getFileStroagePath(int userKey);
	
	public String getFileStroagePath(String path, int userKey);
	
	public String getTemporaryFilename(String filename, String extension);
	
	public File newJavaFile(String filename, int userKey);
	
	public File newJavaFile(String filename, String path, int userKey);
	
	public File readJavaFile(String path);
	
	public FileInfo getFileInfo(int fileKey);
	
	public int insertFileInfo(String name, String extension, String path, String contentType, FileUploadType uploadType, long Size, int userKey);
	
	public void updateFileInfoStatus(int key, DataState state, int userKey);
	
	public FileDownload getFileDownload(int fileDownloadKey);
	
	public int insertFileDownload(int fileKey, int userKey);
	
	public void updateFileDownloadSize(int key, long size, int userKey);
	
	public void updateFileDownloadStatus(int key, DataState state, int userKey);
	
}
