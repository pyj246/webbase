package kr.co.pyj246.webbase.component.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringReader;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.FontProvider;
import com.itextpdf.text.pdf.ICC_Profile;
import com.itextpdf.text.pdf.PdfAConformanceLevel;
import com.itextpdf.text.pdf.PdfAWriter;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.Pipeline;
import com.itextpdf.tool.xml.XMLWorker;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import com.itextpdf.tool.xml.html.CssAppliersImpl;
import com.itextpdf.tool.xml.html.Tags;
import com.itextpdf.tool.xml.parser.XMLParser;
import com.itextpdf.tool.xml.pipeline.css.CssResolverPipeline;
import com.itextpdf.tool.xml.pipeline.end.PdfWriterPipeline;
import com.itextpdf.tool.xml.pipeline.html.HtmlPipeline;
import com.itextpdf.tool.xml.pipeline.html.HtmlPipelineContext;
import com.itextpdf.tool.xml.pipeline.html.ImageProvider;

import kr.co.pyj246.webbase.component.FileManager;
import kr.co.pyj246.webbase.component.PdfManager;
import kr.co.pyj246.webbase.model.file.FileUploadType;
import kr.co.pyj246.webbase.view.pdf.provider.DefaultFontProvider;
import kr.co.pyj246.webbase.view.pdf.provider.DefaultImageProvider;

@Component
public class PdfManagerImpl implements PdfManager {
	
	private final String contentType = "application/pdf";
	
	private final FontProvider defaultFontProvider = new DefaultFontProvider();
	
	private final ImageProvider defaultImageProvider = new DefaultImageProvider();
	
	@Autowired
	private FileManager fileManager;
	
	@Override
	public String getContentType() {
		return contentType;
	}
	
	@Override
	public PdfWriter getPdfWriter(Document document, OutputStream outputStream, boolean isArchive) throws DocumentException, IOException {
		PdfWriter pdfWriter = null;
		if(isArchive) {
			pdfWriter = PdfAWriter.getInstance(document, outputStream, PdfAConformanceLevel.PDF_A_3B);
		} else {
			pdfWriter = PdfWriter.getInstance(document, outputStream);
		}
		pdfWriter.createXmpMetadata();
		return pdfWriter;
	}
	
	@Override
	public void setIccProfile(PdfWriter pdfWriter) throws IOException {
		pdfWriter.setOutputIntents("Custom", "", "http://www.color.org", "sRGB IEC61966-2.1",
				ICC_Profile.getInstance(this.getClass().getClassLoader().getResourceAsStream("/kr/co/pyj246/webbase/view/pdf/icm/sRGB Color Space Profile.icm")));
	}
	
	@Override
	public XMLParser getXMLParser(Document document, PdfWriter pdfWriter) {
		return getXMLParser(document, pdfWriter, defaultFontProvider, defaultImageProvider);
	}
	
	@Override
	public XMLParser getXMLParser(Document document, PdfWriter pdfWriter, FontProvider fontProvider, ImageProvider imageProvider) {
		HtmlPipelineContext htmlContext = new HtmlPipelineContext(new CssAppliersImpl(fontProvider));
	    htmlContext.setTagFactory(Tags.getHtmlTagProcessorFactory());
    	htmlContext.setImageProvider(imageProvider);
	    htmlContext.setAcceptUnknown(true).autoBookmark(true);
        Pipeline<?> pipeline = new CssResolverPipeline(
                XMLWorkerHelper.getInstance().getDefaultCssResolver(true),
                new HtmlPipeline(htmlContext, new PdfWriterPipeline(document, pdfWriter)));
        XMLWorker worker = new XMLWorker(pipeline, true);
        XMLParser parser = new XMLParser(worker);
        return parser;
	};
	
	@Override
	public FontProvider getDefaultFontProvider() {
		return defaultFontProvider;
	}
	
	@Override
	public int writeFile(String filename, String contentsHtml,
			boolean isArchive, int userKey) {
		
		int fileKey = -1;
		
		FileUploadType uploadType = FileUploadType.REPORT;
		String extension = "pdf";
		
		File file = null;
		FileOutputStream fileOutputStream  = null;
		
		try {
			file = fileManager.newJavaFile(fileManager.getTemporaryFilename(filename, extension), uploadType.getDirectory(), userKey);
			fileOutputStream = new FileOutputStream(file);
			
			Document document = new Document();
			PdfWriter pdfWriter = getPdfWriter(document, fileOutputStream, isArchive);
			
			document.open();
			
			if(isArchive) {
				setIccProfile(pdfWriter);
			}
			pdfWriter.setViewerPreferences(PdfWriter.ALLOW_PRINTING | PdfWriter.PageLayoutSinglePage);
			pdfWriter.setViewerPreferences(PdfWriter.PageModeUseOutlines);
			
			XMLParser parser = getXMLParser(document, pdfWriter);
	        parser.parse(new StringReader(contentsHtml));
			
			document.close();
			
			fileKey = fileManager.insertFileInfo(filename, extension, file.getPath(), getContentType(), uploadType, file.length(), userKey);
		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			// Close FileOutputStream
			if(fileOutputStream != null) {
				try {
					fileOutputStream.close();
				} catch(Exception e){
					fileOutputStream = null;
				}
			}
		}
		
		return fileKey;
	}
}
