package kr.co.pyj246.webbase.component.impl;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import kr.co.pyj246.webbase.component.ExcelManager;
import kr.co.pyj246.webbase.component.FileManager;
import kr.co.pyj246.webbase.exception.MessageException;
import kr.co.pyj246.webbase.model.base.excel.PoiType;
import kr.co.pyj246.webbase.model.file.FileUploadType;

@Component
public class ExcelManagerImpl implements ExcelManager {
	
	private final int THRESHOLD = 60000;
	
	private final String contentType = "application/vnd.ms-excel";
	
	@Autowired
	private FileManager fileManager;
	
	@Override
	public String getContentType() {
		return contentType;
	}
	
	@Override
	public Workbook readFile(File file) {
		
		PoiType poiType = PoiType.valueOfExtension(file.getName());
		
		if(poiType == null) {
			throw new MessageException("", "");
		} 
		
		try {
			switch (poiType) {
				case HSSF:
					return new HSSFWorkbook(new FileInputStream(file));
	
				case XSSF:
					return new XSSFWorkbook(new FileInputStream(file));
				
				case SXSSF:
					return new SXSSFWorkbook(new XSSFWorkbook(new FileInputStream(file)));
					
				default:
					throw new MessageException("", "");
					
			}
		} catch (FileNotFoundException e) {
			throw new RuntimeException(e);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public Workbook createWorkbook(PoiType poiType) {
		Workbook workbook = null;
		switch (poiType) {
			case HSSF:
				workbook = new HSSFWorkbook();
				break;

			case XSSF:
				workbook = new XSSFWorkbook();
				break;
			
			case SXSSF:
				workbook = new SXSSFWorkbook();
				break;
	
			default:
				break;
		}
		if(workbook == null) {
			workbook = new HSSFWorkbook();
		}
		return workbook;
	}
	
	@Override
	public Workbook createWorkbook(int rowSize) {
		if(rowSize <= THRESHOLD) {
			return createWorkbook(PoiType.HSSF);
		} else {
			return createWorkbook(PoiType.XSSF);
		}
	}
	
	@Override
	public String getFileExtension(Workbook workbook) {
		PoiType poiType = PoiType.valueOfWorkbook(workbook);
		if(poiType != null) {
			return poiType.getExtension();
		}
		return null;
	}
	
	@Override
	public int writeFile(String filename, Workbook workbook, int userKey) {
		
		int fileKey = -1;
		
		FileUploadType uploadType = FileUploadType.REPORT;
		String extension = getFileExtension(workbook);
		
		File file = null;
		FileOutputStream fileOutputStream  = null;
		
		try {
			file = fileManager.newJavaFile(fileManager.getTemporaryFilename(filename, extension), uploadType.getDirectory(), userKey);
			fileOutputStream = new FileOutputStream(file);
			
			workbook.write(fileOutputStream);
			
			fileKey = fileManager.insertFileInfo(filename, extension, file.getPath(), getContentType(), uploadType, file.length(), userKey);
		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			// Close FileOutputStream
			if(fileOutputStream != null) {
				try {
					fileOutputStream.close();
				} catch(Exception e){
					fileOutputStream = null;
				}
			}
		}
		
		return fileKey;
	}
	
}
