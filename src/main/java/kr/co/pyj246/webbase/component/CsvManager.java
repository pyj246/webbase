package kr.co.pyj246.webbase.component;

import java.io.OutputStream;
import java.util.List;

public interface CsvManager {
	
	public String getContentType();
	
	public void writeOutputStream(OutputStream outputStream, List<List<String>> csvContents) throws Exception;
	
	public int writeFile(String filename, List<List<String>> csvContents, int userKey);
	
}
