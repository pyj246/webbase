package kr.co.pyj246.webbase.component.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import kr.co.pyj246.webbase.component.CsvManager;
import kr.co.pyj246.webbase.component.FileManager;
import kr.co.pyj246.webbase.model.file.FileUploadType;

@Component
public class CsvManagerImpl implements CsvManager {
	
	private final String contentType = "application/CSV";
	
	@Autowired
	private FileManager fileManager;
	
	@Override
	public String getContentType() {
		return contentType;
	}
	
	@Override
	public void writeOutputStream(OutputStream outputStream, List<List<String>> csvContents) throws Exception {
		OutputStreamWriter outputStreamWriter = null;
		CSVPrinter csvPrinter = null;
		
		try {
			// Make CSV Printer
			outputStreamWriter = new OutputStreamWriter(outputStream);
			csvPrinter = new CSVPrinter(outputStreamWriter, CSVFormat.DEFAULT);
			
			// Build CSV Printer
			for(List<String> csvContent : csvContents) {
				csvPrinter.printRecord(csvContent);
			}
			
		} finally {
			csvPrinter.close();
		}
	}
	
	@Override
	public int writeFile(String filename, List<List<String>> csvContents, int userKey) {
		
		int fileKey = -1;
		
		FileUploadType uploadType = FileUploadType.REPORT;
		String extension = "csv";
		
		File file = null;
		FileOutputStream fileOutputStream  = null;
		
		try {
			file = fileManager.newJavaFile(fileManager.getTemporaryFilename(filename, extension), uploadType.getDirectory(), userKey);
			fileOutputStream = new FileOutputStream(file);
			
			writeOutputStream(fileOutputStream, csvContents);
			
			fileKey = fileManager.insertFileInfo(filename, extension, file.getPath(), getContentType(), uploadType, file.length(), userKey);
		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			// Close FileOutputStream
			if(fileOutputStream != null) {
				try {
					fileOutputStream.close();
				} catch(Exception e){
					fileOutputStream = null;
				}
			}
		}
		
		return fileKey;
	}
	
}
