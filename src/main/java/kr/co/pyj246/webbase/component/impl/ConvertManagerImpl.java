package kr.co.pyj246.webbase.component.impl;

import java.io.IOException;
import java.util.Map;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import kr.co.pyj246.webbase.component.ConvertManager;

@Component
public class ConvertManagerImpl implements ConvertManager {
	
	@Autowired
	private ObjectMapper objectMapper;
	
	@Override
	public int convertObjectIntoInt(Object object) {
		int result = -1;
        if(object != null) {
            if(object instanceof Integer) {
                result = ((Integer) object).intValue();
            } else {
                String strObject = (String) object;
                if(strObject != null && !strObject.trim().isEmpty()
                        && Pattern.compile("^(-)?[0-9]*(\\.0)?$").matcher(strObject).find()) {
                    result = Integer.valueOf(strObject);
                }
            }
        }
		return result;
	}
	
	@Override
	public boolean convertObjectIntoBoolean(Object object) {
		boolean result = false;
		if(object != null) {
			if(object instanceof Boolean) {
				result = ((Boolean) object).booleanValue();
			} else {
				result = Boolean.valueOf((String) object);
			}
		}
		return result;
	}
	
	@Override
	public String convertObjectIntoJson(Object object) {
		String jsonText = null;
        try {
            jsonText = objectMapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
        return jsonText;
	}
	
	@Override
	public Map<String, Object> convertJsonStringIntoMap(String jsonString) {
		Map<String, Object> object = null;
		try {
			object =  objectMapper.readValue(jsonString, new TypeReference<Map<String, Object>>() {});
		} catch (IOException e) {
            throw new RuntimeException(e);
		}
		return object;
	}
	
}
