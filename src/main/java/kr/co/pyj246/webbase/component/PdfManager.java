package kr.co.pyj246.webbase.component;

import java.io.IOException;
import java.io.OutputStream;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.FontProvider;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.parser.XMLParser;
import com.itextpdf.tool.xml.pipeline.html.ImageProvider;

public interface PdfManager {
	
	public String getContentType();
	
	public PdfWriter getPdfWriter(Document document, OutputStream outputStream, boolean isArchive) throws DocumentException, IOException;
	
	public void setIccProfile(PdfWriter pdfWriter) throws IOException;
	
	public XMLParser getXMLParser(Document document, PdfWriter pdfWriter);
	
	public XMLParser getXMLParser(Document document, PdfWriter pdfWriter, FontProvider fontProvider, ImageProvider imageProvider);
	
	public FontProvider getDefaultFontProvider();
	
	public int writeFile(String filename, String contentsHtml, boolean isArchive, int userKey);
}
