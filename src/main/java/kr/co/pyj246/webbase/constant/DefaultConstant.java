package kr.co.pyj246.webbase.constant;

public class DefaultConstant {
	
	public static final String SESSION_USER = "AUTHENTICATION_SESSION_USER";
	
	public static final String AUTHENTICATION_REDIRECT_URL = "AUTHENTICATION_REDIRECT_URL";
	
	public static final String DOWNLOAD_FILE_KEY = "DOWNLOAD_FILE_KEY";
	
	public static final String DOWNLOAD_USER_KEY = "DOWNLOAD_USER_KEY";
	
	public static final String DOWNLOAD_FILE_NAME = "DOWNLOAD_FILE_NAME";

	public static final String DOWNLOAD_FILE_CONTENTS = "DOWNLOAD_FILE_CONTENTS";
	
	public static final String DOWNLOAD_PDF_IS_ARCHIVE = "DOWNLOAD_PDF_IS_ARCHIVE";
	
	public static final String DOWNLOAD_COMPLETE_COOKIE = "DOWNLOAD_COMPLETE_COOKIE";
	
}
