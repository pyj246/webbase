<div class="modal fade bs-example-modal-sm" id="progress-modal" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">
                    <span class="glyphicon glyphicon-time"></span>
                    ${message("MESSAGE.WAIT")}
                 </h4>
            </div>
            <div class="modal-body">
                <div class="progress">
                    <div class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" style="min-width:3em;width:0%;">0%</div>
                </div>
            </div>
        </div>
    </div>
</div>

