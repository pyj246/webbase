<div class="container">
	<form id="loginForm" action="/login/check" method="POST" data-ajaxtype="json" data-serialize="true">
		<div id="login-input">
			<div class="input-group input-group-lg">
			  	<span class="input-group-addon">${message("LOGIN.EMAIL")}</span>
			  	<input type="email" name="userId" id="userId" class="form-control" placeholder="${message("LOGIN.EMAIL")}" autofocus>
			</div>
			<div class="input-group input-group-lg">
			  	<span class="input-group-addon">${message("LOGIN.PASSWORD")}</span>
			  	<input type="password" name="password" id="password" class="form-control" placeholder="${message("LOGIN.PASSWORD")}">
			</div>
     	</div>
     	<br />
		<input type="submit" class="btn btn-lg btn-primary btn-block" value="${message("BUTTON.LOGIN")}">
		<br />
		<div id="login-error" class="alert alert-danger" style="display:none;">&nbsp;</div>
	</form>
</div>
