<#import "/spring.ftl" as spring/>

<#function message id="">
	<#local returnValue = "" />
	<#attempt>
		<#local returnValue><@spring.message id /></#local>
	<#recover>
		<#if id?? && id?length &gt; 0>
			<#local returnValue = id />
		</#if>	
	</#attempt>
    <#return returnValue>
</#function>

<#function menu index=-1>
	<#local returnValue="" />
	<#if springMacroRequestContext.requestUri??>
		<#local menuArray = springMacroRequestContext.requestUri?split("/") />
		<#if menuArray?size &gt; 1>
			<#if index &lt; 0>
				<#local returnValue = menuArray />			
			<#else>
				<#local index = index + 1 />
				<#local returnValue = menuArray[index] />
			</#if>
		</#if>	
	</#if>
	<#return returnValue>
</#function>

<#macro getFileHtml file="">
	<#local fileKey = "#\{fileKey}#"
			fileName = "#\{fileName}#"
			fileSize = "#\{fileSize}#"/>
	<#if file != "">
		<#local fileKey = file["fileKey"]
				fileName = file["fileName"]
				fileSize = file["fileSize"]/>
	<div data-filekey="${fileKey}">
	<#else>			
	<div id="fileTemplate">
	</#if>
		<input type="hidden" value="${fileKey}"/>
		<div class="col-sm-4">${fileName}</div>
		<div class="text-right col-sm-2">${fileSize}</div>
		<div class="col-sm-4">
			<div role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0" class="progress progress-striped active">
				<div class="progress-bar progress-bar-success" style="width:0%;"></div>
			</div>
		</div>
		<div class="text-left col-sm-1"><label class="btn btn-danger btn-xs">취소</label></div>
	</div>
</#macro>