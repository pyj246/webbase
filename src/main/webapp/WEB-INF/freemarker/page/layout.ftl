<#include "base.ftl"/>
<#import "base.ftl" as base/>
<#setting number_format="#">
<#assign security=JspTaglibs["http://www.springframework.org/security/tags"] />

<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko-KR" lang="ko-KR">	
	
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="_locale" content="${.locale}"/>
		<meta name="url" content="${springMacroRequestContext.requestUri}"/>
		<#--<meta name="firstMenu" content="${springMacroRequestContext.requestUri?substring(1, springMacroRequestContext.requestUri?index_of('/', 1))}"/>-->
		<@security.csrfMetaTags />
		<title>${message("APPLICATION_NAME")}</title>
		<link rel="icon" type="image/ico" href="/resources/favicon.ico">
		<link href="/resources/css/base.css" rel="stylesheet" type="text/css" />
	</head>
	
	<body>
	
		<#-- 로그인 페이지 시작 -->
		<@security.authorize access="not hasRole('ROLE_USER')">
			<#include "access/login.ftl"/>
		</@security.authorize>
		<#-- 로그인 페이지 끝 -->
		
		<#-- 페이지 시작 -->
		<@security.authorize access="hasRole('ROLE_USER')">
			<#include "header.ftl"/>
			<div class="container">
				<#include contentPath/>
			</div>
		</@security.authorize>
		<#-- 페이지 끝 -->
		
		<#include "progress.ftl"/>
		<#include "message.ftl"/>
		<div id="templateHtml" class="hidden">
			<@base.getFileHtml ""/>			
		</div>
		<div>
			<script type="text/javascript" src="/resources/js/library/jquery-1.11.2.min.js"></script>
			<script type="text/javascript" src="/resources/js/library/jquery-ui-1.11.4.min.js"></script>
			<script type="text/javascript" src="/resources/js/library/jquery.json-2.4.min.js"></script>
			<script type="text/javascript" src="/resources/js/library/jquery.fileupload-5.42.3.js"></script>
			<script type="text/javascript" src="/resources/js/library/bootstrap-3.3.4.min.js"></script>
			<script type="text/javascript" src="/resources/js/library/html5shiv-3.7.0.min.js"></script>
			<script type="text/javascript" src="/resources/js/library/respond.js"></script>
			<script type="text/javascript" src="/resources/js/library/moment-with-locales-2.10.3.js"></script>
			<script type="text/javascript" src="/resources/js/library/moment-timezone-with-data-0.4.0.min.js"></script>
			<script type="text/javascript" src="/resources/js/library/bootstrap-datetimepicker-4.14.30.js"></script>
			<script type="text/javascript" src="/resources/js/library/summernote-0.6.9.min.js"></script>
			<script type="text/javascript" src="/resources/js/plugin/base.js"></script>
			<script type="text/javascript" src="/resources/js/plugin/file.js"></script>
			<script type="text/javascript" src="/resources/js/plugin/form.js"></script>
			<#assign jsName = menu(0) />
			<#if jsName == "">
				<#assign jsName = "home" />
			</#if>
			<script type="text/javascript" src="/resources/js/page/${jsName}.js"></script>
		</div>
	</body>
	
</html>