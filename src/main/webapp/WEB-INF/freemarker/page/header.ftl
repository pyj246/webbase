<nav class="navbar navbar-inverse">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
	        </button>
			<a class="navbar-brand">${message("APPLICATION_NAME")}</a>
		</div>
		<div id="navbar" class="navbar-collapse collapse">
          	<ul class="nav navbar-nav">
	            <li <#if menu(0) == ''>class="active"</#if>>
	            	<a href="/"><span class="glyphicon glyphicon-home"></span> ${message("MENU.HOME")}</a>
	            </li>
	            <li class="dropdown<#if menu(0) == 'edit'> active</#if>" id="header-edit">
	            	<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-edit"></span> ${message("MENU.EDIT")} <span class="caret"></span></a>
	            	<ul class="dropdown-menu">
		                <li><a href="/edit/formserialize">${message("MENU.EDIT.FORM_SERIALIZE")}</a></li>
		                <li><a href="/edit/ajaxserialize">${message("MENU.EDIT.AJAX_SERIALIZE")}</a></li>
		                <li><a href="/edit/ajaxjson">${message("MENU.EDIT.AJAX_JSON")}</a></li>
					</ul>
	            </li>
	            <li class="dropdown" id="header-download">
	            	<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-download-alt"></span> ${message("MENU.DOWNLOAD")} <span class="caret"></span></a>
	            	<ul class="dropdown-menu">
		                <li><a href="/download/csv" onclick="$.file.download('/download/csv');return false;">${message("DOWNLOAD.CSV")}</a></li>
		                <li><a href="/download/excel" onclick="$.file.download('/download/excel');return false;">${message("DOWNLOAD.EXCEL")}</a></li>
		                <li><a href="/download/pdf" onclick="$.file.download('/download/pdf');return false;">${message("DOWNLOAD.PDF")}</a></li>
		                <li><a href="/download/pdfa" onclick="$.file.download('/download/pdfa');return false;">${message("DOWNLOAD.PDFA")}</a></li>
		                <li><a href="/download/file" onclick="$.file.download('/download/file');return false;">${message("DOWNLOAD.FILE")}</a></li>
					</ul>
	            </li>
         	</ul>
			<ul class="nav navbar-nav navbar-right">
				<li class="dropdown">
	            	<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> ${userInfo.email} <span class="caret"></span></a>
	            	<ul class="dropdown-menu">
		                <li class="divider"></li>
		                <li><a href="#" id="header-logout">${message("BUTTON.LOGOUT")}</a></li>
					</ul>
				</li>
				<li class="dropdown">
	            	<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-time"></span> <span id="heade-timer-expire">30:00</span> <span class="caret"></span></a>
	            	<ul class="dropdown-menu">
		                <li><a href="#" id="header-timer-reset">Reset</a></li>
						<#--<li><a href="#" id="header-timer-disable">Disable</a></li>-->
					</ul>
				</li>
				<li class="dropdown">
	            	<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-globe"></span> ${message("LANGUAGE")} <span class="caret"></span></a>
	            	<#assign languages = {"ko": message("LANGUAGE.KOREAN"), "en": message("LANGUAGE.ENGLISH")} />
	            	<ul class="dropdown-menu" id="header-language">
	            		<#list languages?keys as language>
	            			<#if language != .locale>
	            			<li data-language="${language}"><a href="#" onclick="return false;">${languages[language]}</a></li>
	            			</#if>
	            		</#list>
					</ul>
	            </li>
			</ul>
        </div>
	</div>
</nav>