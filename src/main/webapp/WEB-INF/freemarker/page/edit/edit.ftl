<form action="${springMacroRequestContext.requestUri + '/submit'}" id="editForm" method="POST" class="form-horizontal" <#if springMacroRequestContext.requestUri?contains("/ajax")> data-ajaxtype="json"</#if> <#if springMacroRequestContext.requestUri?contains("serialize")> data-serialize=true</#if>>
	<div class="form-group">
		<label class="control-label col-sm-2">Static Text</label>
		<div class="col-sm-10">
			<#assign staticText = ""/>
			<#if springMacroRequestContext.requestUri?contains("/formserialize")>
				<#assign staticText = message("MENU.EDIT.FORM_SERIALIZE")/>
			<#elseif springMacroRequestContext.requestUri?contains("/ajaxserialize")>
				<#assign staticText = message("MENU.EDIT.AJAX_SERIALIZE")/>
			<#elseif springMacroRequestContext.requestUri?contains("/ajaxjson")>
				<#assign staticText = message("MENU.EDIT.AJAX_JSON")/>
			</#if>
			<p class="form-control-static">${staticText!""}</p>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-sm-2">${message("MENU.EDIT.LABEL.INPUT_TEXT")}</label>
		<div class="col-sm-10">
			<input type="text" id="editInputText" name="editInputText" placeholder="${message('PLACEHOLDER.TEXT')}" class="form-control col-sm-2"/>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-sm-2">${message("MENU.EDIT.LABEL.DATE_TIME")}</label>
		<div class="col-sm-3">
			<div class="input-group date">
                <input type="text" id="editInputTextDateTime" name="editInputTextDateTime" data-type="datetime" data-format="DD/MMM/YYYY HH:mm" class="form-control"/>
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
            </div>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-sm-2">${message("MENU.EDIT.LABEL.DATE")}</label>
		<div class="col-sm-2">
			<div class="input-group date">
                <input type="text" id="editInputTextDate" name="editInputTextDate" data-type="date" data-format="DD/MMM/YYYY" class="form-control"/>
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
            </div>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-sm-2">${message("MENU.EDIT.LABEL.TIME")}</label>
		<div class="col-sm-2">
			<div class="input-group date">
                <input type="text" id="editInputTextTime" name="editInputTextTime" data-type="time" data-format="HH:mm" class="form-control" />
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-time"></span>
                </span>
            </div>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-sm-2">${message("MENU.EDIT.LABEL.TEXT_AREA")}</label>
		<div class="col-sm-10">
			<textarea id="editTextarea" name="editTextarea" class="form-control col-sm-2"></textarea>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-sm-2">${message("MENU.EDIT.LABEL.TEXT_EDITOR")}</label>
		<div class="col-sm-10">
			<textarea data-type="texteditor" id="editTextEditor" name="editTextEditor" class="hidden"></textarea>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-sm-2">${message("MENU.EDIT.LABEL.SELECT")}</label>
		<div class="col-sm-10">
			<select id="editSelect" name="editSelect" class="form-control col-sm-2">
				<option value="1">option 1</option>
				<option value="2">option 2</option>
				<option value="3">option 3</option>
				<option value="4">option 4</option>
				<option value="5">option 5</option>
			</select>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-sm-2">${message("MENU.EDIT.LABEL.SELECT_MULTIPLE")}</label>
		<div class="col-sm-10">
			<select multiple id="editSelectMultiple" name="editSelectMultiple" class="form-control col-sm-2">
				<option value="1">option 1</option>
				<option value="2">option 2</option>
				<option value="3">option 3</option>
				<option value="4">option 4</option>
				<option value="5">option 5</option>
			</select>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-sm-2">${message("MENU.EDIT.LABEL.CHECK")}</label> 
		<div class="col-sm-10">
			<div class="checkbox">
				<label> <input type="checkbox" id="editInputCheck" name="editInputCheck" value="true"/> Active</label>
			</div>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-sm-2">${message("MENU.EDIT.LABEL.CHECK_MULTIPLE")}</label> 
		<div class="col-sm-10">
			<label class="checkbox-inline col-sm-2"> <input type="checkbox" id="editInputCheckMultiple" name="editInputCheckMultiple" value="1"/> Apple</label>
			<label class="checkbox-inline col-sm-2"> <input type="checkbox" id="editInputCheckMultiple" name="editInputCheckMultiple" value="2"/> Banana</label>
			<label class="checkbox-inline col-sm-2"> <input type="checkbox" id="editInputCheckMultiple" name="editInputCheckMultiple" value="3"/> Melon</label>
			<label class="checkbox-inline col-sm-2"> <input type="checkbox" id="editInputCheckMultiple" name="editInputCheckMultiple" value="4"/> Cherry</label>
			<label class="checkbox-inline col-sm-2"> <input type="checkbox" id="editInputCheckMultiple" name="editInputCheckMultiple" value="5"/> Orange</label>
			<label class="checkbox-inline col-sm-2"> <input type="checkbox" id="editInputCheckMultiple" name="editInputCheckMultiple" value="6"/> Water Melon</label>
			<label class="checkbox-inline col-sm-2"> <input type="checkbox" id="editInputCheckMultiple" name="editInputCheckMultiple" value="7"/> Lemon</label>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-sm-2">${message("MENU.EDIT.LABEL.RADIO")}</label> 
		<div class="col-sm-10">
			<label class="radio-inline col-sm-2"> <input type="radio" id="editInputRadio" name="editInputRadio" value="1"/> Apple</label>
			<label class="radio-inline col-sm-2"> <input type="radio" id="editInputRadio" name="editInputRadio" value="2"/> Banana</label>
			<label class="radio-inline col-sm-2"> <input type="radio" id="editInputRadio" name="editInputRadio" value="3"/> Melon</label>
			<label class="radio-inline col-sm-2"> <input type="radio" id="editInputRadio" name="editInputRadio" value="4"/> Cherry</label>
			<label class="radio-inline col-sm-2"> <input type="radio" id="editInputRadio" name="editInputRadio" value="5"/> Orange</label>
			<label class="radio-inline col-sm-2"> <input type="radio" id="editInputRadio" name="editInputRadio" value="6"/> Water Melon</label>
			<label class="radio-inline col-sm-2"> <input type="radio" id="editInputRadio" name="editInputRadio" value="7"/> Lemon</label>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-sm-2">File</label> 
		<div class="col-sm-10">
			<label for="editInputFile" class="btn btn-default btn-sm">${message("BUTTON.FILE")}</label>
			<input type="file" id="editInputFile" name="editInputFile" multiple="true" class="hidden" data-filetype="/(\.|\/)(gif|jpe?g|png)$/i" data-mincount="1" data-maxcount="3" data-maxsize="1000"/>
		</div>
	</div>
	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-10">
			<button type="submit" class="btn btn-primary btn-lg">${message("BUTTON.SUBMIT")}</button>
		</div>
	</div>
</form>