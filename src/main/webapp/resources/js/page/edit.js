(function($) {
	'use strict';
	
	$(document.body).ready(function() {
		// form init
		$.form.init($('#editForm'), true);
	});

})(jQuery);