(function($) {
	'use strict';

	$(document.body).ready(function() {
		var $loginForm = $('#loginForm');
		
		// form Init
		$.form.init($loginForm);
		
		// Before Submit 
		$loginForm.on('beforeSubmit', function(event) {
			var $form = $(event.target),
				$userId = $form.find('[name=userId]'),
				$password = $form.find('[name=password]'),
				userId = $.form.getValue($userId),
				password = $.form.getValue($password),
				isActive = true;
			if($.base.isEmpty(userId)) {
				$form.find('#login-error').text($.base.message('MESSAGE.INPUT', {'#{INPUT}#':$.base.message('LOGIN.EMAIL')})).show();
				isActive = false;
			} else if($.base.isEmpty(password)) {
				$form.find('#login-error').text($.base.message('MESSAGE.INPUT', {'#{INPUT}#':$.base.message('LOGIN.PASSWORD')})).show();
				isActive = false;
			}
			$form.data('active', isActive);
		});
		
		// Success Submit
		$loginForm.on('successSubmit', function(event, data, textStatus, jqXHR) {
			var $form = $(event.target),
				result = data['result'];
			if(result === 'fail') {
				$form.data('active', false);
				$form.find('#login-error').text($.base.message('MESSAGE.LOGIN_FAIL')).show();
			}
		});
		
	});
	
	

})(jQuery);