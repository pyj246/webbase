(function($) {
	'use strict';
	
	/**
	 * Form Functions
	 */
	$.form = {
		init: function(formElement, checkChange) {
			var $form = $(formElement);
			
			// Regist Date & Time
			$form.find('input[data-type="datetime"],input[data-type="date"],input[data-type="time"]').each(function(i, input) {
				var $input = $(input),
					$parentDiv = $input.parent(),
					language = $('meta[name="_locale"]').attr('content'),
					type = $input.data('type'), 
					format = $input.data('format'),
					raw = $input.data('raw'),
					defaultFormat = undefined,
					defaultDate = undefined;
				
				var defaultDateFormat = 'YYYY-MM-DD',
					defaultTimeFormat = 'HH:mm';
				
				var _changeDateTime = function() {
					var raw = moment($input.val(), format, language).format(defaultFormat);
					if(raw === 'Invalid date') {
						raw = '';
					}
					$input.data('raw', raw);
					$input.trigger('dp.changeAfter');
				}
				
				switch (type) {
					case 'datetime':
						defaultFormat = defaultDateFormat + ' ' + defaultTimeFormat;
						break;
					
					case 'date':
						defaultFormat = defaultDateFormat;
						break;
					
					case 'time':
						defaultFormat = defaultTimeFormat;
						break;	
		
					default:
						break;
				}
				
				if($.base.isEmpty(format)) {
					format = defaultFormat;
				}
				
				if($.base.isNotEmpty(raw) && moment(raw, defaultFormat, language).format(format) !== 'Invalid date') {
					defaultDate = moment(raw, defaultFormat, language);
				}
				
				$parentDiv.datetimepicker({
					'useStrict': true,
					'locale': language,
					'format': format,
					'defaultDate': defaultDate,
					'showTodayButton': true,
					'showClear': true,
					'showClose': true
				});
				
				// Regist Event
				$parentDiv.on('dp.change', function(event) {
					_changeDateTime();
				}).on('dp.error', function(event) {
					_changeDateTime();
				});
				
			});

			// Regist Text Editor
			$form.find('textarea[data-type="texteditor"]').each(function(i, textarea) {
				var $textarea = $(textarea).hide();
				$textarea.summernote({
					'height':'150px'
				});
				$textarea.code($textarea.val());
			});
			
			// Check Change
			if(checkChange === true) {
				var originalFormData = $.form.getFormData($form);
				
				// 변경에 대한 처리를 하는 기능
				var	_changeElement = function(event, originalValue, currentValue) {
						var element = event.target,
							changeData = $(document.body).data('changeData');
						if($.base.isEmpty(originalValue)) {
							originalValue = undefined;
						}
						if($.base.isEmpty(currentValue)) {
							currentValue = undefined;
						}
						if(changeData === undefined) {
							changeData = [];
						}
						if(originalValue === currentValue) {
							if($.inArray(element, changeData) > -1) {
								changeData.splice($.inArray(element, changeData), 1);
							}
						} else {
							if($.inArray(element, changeData) < 0) {
								changeData.push(element)
							}
						}
						$(document.body).data('changeData', changeData);
					}
				
				// Regist Event
				$form.on('change', 'input[type!=file],textarea,select', function(event) {
					var originalValue = originalFormData[$(event.target).prop('name')],
						currentValue = $.form.getValue(event.target);
					_changeElement(event, originalValue, currentValue);
					
				}).on('dp.changeAfter', 'input', function(event) {
					var originalValue = originalFormData[$(event.target).prop('name')],
						currentValue = $.form.getValue(event.target);
					_changeElement(event, originalValue, currentValue);
					
				}).on('summernote.change', 'textarea[data-type="texteditor"]', function(event) {
					var originalValue = originalFormData[$(event.target).prop('name')],
						currentValue = $(event.target).code();
					_changeElement(event, originalValue, currentValue);
					
				});
			}
		},
		// Get Element Value
		getValue: function(element, defaultValue) {
			var $element = $(element),
				tag = $element.prop('tagName'),
				type = $element.prop('type'),
				raw = $element.data('raw'),
				val = $element.val(),
				result = undefined;
			
			switch(tag) {
				case 'INPUT':
					switch(type) {
						case 'checkbox':
							var val = $element.attr('value'),
								isChecked = $element.is(':checked');
							if(raw === undefined && val === undefined) {
								result = isChecked;	// 정의된 값이 없으면 Boolean으로 판단한다.
							} else if(isChecked) {
								result = _getResult(val, raw);
							}
							break;
							
						case 'radio':
							if($element.is(':checked')) {
								result = _getResult(val, raw);
							}
							break;
					
						default:
							result = _getResult(val, raw);
							break;
					}
					
					break;
	
				case 'SELECT':
					var $selectedOption = $element.find('option:selected');
					if($selectedOption.length === 1) {
						result = _getResult(val, $selectedOption.data('raw'));
					} else if($selectedOption.length > 1) {
						var result = [];
						$selectedOption.each(function(i, option) {
							var $option = $(option),
								optionVal = $option.val(),
								optionRaw = $option.data('raw');
							result.push(_getResult(optionVal, optionRaw));
						});
					}
					break;
					
				default:
					result = _getResult(val, raw);
					break;
			}
			
			if($.base.isEmpty(result) && $.base.isNotEmpty(defaultValue)) {
				result = defaultValue;
			}
			
			return result;
			
			function _getResult(val, raw) {
				if(raw !== undefined) {
					return raw;
				} else {
					return val;
				}
			}
		},
		// Get Form Data
		getFormData: function(formElement) {
			var formData = {};
			$(formElement).find('input[type!=file],textarea,select').each(function(i, element) {
				var name = $(element).prop('name'),
					value = $.form.getValue(element),
					isTextEditorElement = $(element).parents('.note-editor').length > 0;
				if(isTextEditorElement === false) {
					if(formData[name] !== undefined) {
						var originalValue = formData[name];
						if($.isArray(originalValue) && $.isArray(value)) {
							formData[name] = $.merge(originalValue, value);
						} else if($.isArray(originalValue)) {
							originalValue.push(value);
							formData[name] = originalValue;
						} else if($.isArray(value)){
							formData[name] = $.merge([originalValue], value);
						} else {
							formData[name] = [originalValue, value];
						}
					} else {
						formData[name] = value;
					}
				}
			});
			return formData;
		}
	}
	
	/**
	 * Document Form Submit
	 */
	$(document.body).on('submit', 'form', function(event, onlySubmit) {
		
		// 처리없이 바로 Form Submit 실행
		if(onlySubmit === true) {
			return true;
		}
		
		// Upload Files & Submit
		try {
			var $form = $(event.target),
				url = $form.prop('action'),
				type = $form.prop('method'),
				dataType = $form.data('ajaxtype'),
				serialize = $form.data('serialize'),
				$modal = $('#progress-modal'),
				$progressbar = $modal.find('.progress-bar');
			
			if($form.data('running') === true) {
				return false;
			}
			
			// Modal
			$form.data('running', true);
			$progressbar.css('width', '100%').text('');
			$modal.modal('show');
			
			// remove Data - Active 
			$form.removeData('active')
			
			// Before Submit
			$form.trigger('beforeSubmit');
			if($form.data('active') === false) {
				$form.removeData('active');
				$form.removeData('running');
				return false;
			}
			
			// Date & Time 처리
			if($('input[data-type="datetime"],input[data-type="date"],input[data-type="time"]').length >0) {
				$('input[data-type="datetime"],input[data-type="date"],input[data-type="time"]').each(function(i, input) {
					var $input = $(input);
					$input.val($.form.getValue(input));
				});
			}
			
			// 텍스트 에디터 처리
			if($form.find('textarea[data-type="texteditor"]').length > 0) {
				$form.find('textarea[data-type="texteditor"]').each(function(i, textarea) {
					var $textarea = $(textarea);
					$textarea.val($textarea.code());
				});
			}
			
			// 파일이 있으면 파일을 처리한다.
			if($form.find('input:file').length > 0) {
				var fileSequence = 0;
				$form.find('input:file').each(function(i, inputFile) {
					$.each($(inputFile).nextAll(), function(j, next) {
						if($(next).hasClass('help-block')) {
							$.each($(next).children('div'), function(k, fileElement) {
								var $fileElement = $(fileElement),
									fileKey = $fileElement.data('filekey'),
									fileData = $fileElement.data('file');
								if($.base.isEmpty(fileKey) && $.base.isNotEmpty(fileData)) {
									var formData = {};
									formData[$('meta[name="_csrf_parameter"]').attr('content')] = $('meta[name="_csrf"]').attr('content');
									formData['index'] = fileSequence++;
									fileData['formData'] = formData;
									fileData['fileElement'] = fileElement;
									fileData.submit();
								}
							});
						}
					});
					$(inputFile).remove();
				});
				
				// 주기적(1초)으로 체크하여 파일업로드가 완료되면 Submit을 실행한다. 
				var fileuploadCheckInterval = setInterval(function() {
					var completeFileCount = $form.data('completeFileCount');
					if(isNaN(completeFileCount)) {
						completeFileCount = 0;
					}
					if(fileSequence === parseInt(completeFileCount)) {
						clearInterval(fileuploadCheckInterval);
						_executeSubmit();
					}
				}, 1000);
			} else {
				// Execute Submit
				_executeSubmit();
			}
			
		} catch(exeption) {
			alert('$.baseAjax Error : "' + exeption + '"');
			console.log(exeption);
			console.log(exeption.stack);
			throw exeption;
			return false;
		} finally {
			return false;
		}
		
		// 실제로 Submit을 처리
		function _executeSubmit() {
			// Submit 전에 Change Data를 삭제한다.
			$(document.body).removeData('changeData');
			
			if(typeof dataType === 'string' && $.inArray(dataType.toLowerCase(), ['xml', 'json', 'script', 'html'])) {
				// Execute Ajax
				try {
					var ajaxSettings = {
						url: '/',
						type: 'POST',
						dataType: dataType.toLowerCase(),
						isSubmit: true,
						$form:$form
					},
					formData = $.form.getFormData($form);
					
					// url
					if(typeof url === 'string') {
						ajaxSettings['url'] = url;
					}
					
					// type (method)
					if(typeof type === 'string' && $.inArray(type.toUpperCase(), ['POST', 'GET'])) {
						ajaxSettings['type'] = type.toUpperCase();
					}
					
					// Data
					if(serialize === 'true' || serialize === true) {
						ajaxSettings['data'] = $.param(formData);
					} else {
						ajaxSettings['data'] = {'dataJson':JSON.stringify(formData)};
					}
					
					// Submit Ajax
					$.base.ajax(ajaxSettings);
				} catch(exeption) {
					alert('_executeSubmit Error : "' + exeption + '"');
					console.log(exeption);
					console.log(exeption.stack);
					throw exeption;
				} finally {
					return false;
				}
			} else {
				if($form.find('[name=' + $('meta[name="_csrf_parameter"]').attr('content') + ']').length < 1) {
					$form.append('<input type="hidden" name="' + $('meta[name="_csrf_parameter"]').attr('content') + '" value="' + $('meta[name="_csrf"]').attr('content') + '">');
				}
				$form.trigger('submit', true);
			}
		}
	});
	
})(jQuery);