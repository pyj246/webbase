(function($) {
	'use strict';
	
	/**
	 * Base Functions
	 */
	$.base = {
		// Relace Template
		replaceTemplate: function(template, model) {
			return template.replace(/#{([A-Z_]+)}#/gi, function () {
				var target = arguments[1];
                if (model[target] !== undefined && model[target] !== null && model[target] !== '') {
                    var replaceValue = '';
                    if ($.isArray(model[target])) {
                        replaceValue = model[target].join('');
                    } else {
                        replaceValue = model[target] + '';
                    }
                    return replaceValue;
                }
                return '';
            });
		},
		// Check Empty
		isEmpty: function(value) {
			var type = typeof value,
				result = true;
			switch (type) {
				case 'string':
					if(value.length > 0) {
						result = false;
					}
					break;
				
				case 'object':
					if($.isArray(value) && value.length > 0) {
						result = false;
					} else {
						result = false;
					}
					break;
					
				case 'number':
				case 'function':
				case 'boolean':
					result = false;
					break;
					
				default:
					break;
			}
			return result;
		},
		// Check Not Empty
		isNotEmpty: function(value) {
			return !$.base.isEmpty(value);
		},
		// Ajax
		ajax: function(ajaxSettings) {
			if(typeof ajaxSettings['type'] !== 'string') {
				ajaxSettings['type'] = 'POST'
			}
			if(typeof ajaxSettings['dataType'] !== 'string') {
				ajaxSettings['dataType'] = 'json'
			}
			if(typeof ajaxSettings['async'] !== 'boolean') {
				ajaxSettings['async'] = false;
			}
			
			// Ajax Event
			var beforeSend = ajaxSettings['beforeSend'],
				success = ajaxSettings['success'],
				error = ajaxSettings['error'],
				complete = ajaxSettings['complete'];
			
			// beforeSend
			ajaxSettings['beforeSend'] = function(jqXHR, settings) {
				jqXHR.setRequestHeader($('meta[name="_csrf_header"]').attr('content'), $('meta[name="_csrf"]').attr('content'));
				if(typeof beforeSend === 'function') {
					beforeSend(jqXHR, settings);
				}
			}
			
			// Success
			ajaxSettings['success'] = function(data, textStatus, jqXHR) {
				if(typeof success === 'function') {
					success(data, textStatus, jqXHR);
				}
				if(this['isSubmit'] === true) {
					var $form = this['$form'];
					// After Submit
					$form.trigger('successSubmit', data, textStatus, jqXHR);
					if($form.data('active') !== false) {
						if($form !== undefined) {
							$form.removeData('active');
						}
						if($.base.isNotEmpty(data) && $.base.isNotEmpty(data['redirectUrl'])) {
							location.href = data['redirectUrl'];
						}
						if($.base.isNotEmpty(data) && $.base.isNotEmpty(data['reload']) === true) {
							location.reload();
						}
					}
				}
			}
			
			// Error
			ajaxSettings['error'] = function(jqXHR, textStatus, errorThrown) {
				if(typeof error === 'function') {
					error(jqXHR, textStatus, errorThrown);
				} else {
					// TODO 에러 처리
				}
			}
			
			// Complete
			ajaxSettings['complete'] = function(jqXHR, textStatus) {
				var $form = this['$form'],
					$modal = $('#progress-modal'),
					isFileProcessing = $(document.body).data($.file.FILE_PROCESSING);
				if(typeof complete === 'function') {
					complete(jqXHR, textStatus);
				}
				
				// 파일 처리중이면 처리중 모달을 닫지 않는다. plugin/file.js에서 처리한다.
				if(isFileProcessing !== true) {
					$modal.modal('hide');
				}
				
				// Remove Running Data
				if($form !== undefined) {
					$form.removeData('running');
				}
			}
			
			$.ajax(ajaxSettings);
		},
		// Message
		messageInit: false,
		messageDataKey: 'base-message-data',
		messagelanguageKey: 'base-message-language',
		messageTimeKey: 'base-message-time',
		messageData: undefined,
		message: function (id, replacement) {
			// Locale Message Load
			if($.base['messageInit'] === undefined || $.base['messageInit'] === false) {
				$.base['messageInit'] = true;
				var language = $('meta[name="_locale"]').attr('content'),
					messageData = sessionStorage.getItem($.base['messageDataKey']),
					messageLanguage = sessionStorage.getItem($.base['messagelanguageKey']),
					messageTime = parseInt(sessionStorage.getItem($.base['messageTimeKey']));
				// 로케일 파일 로딩 (데이터가 존재하면 5분마다 갱신)
				var reload = false;
				if(language !== messageLanguage
						|| isNaN(messageTime) 
						|| Math.ceil(new Date().getTime() / 1000) - messageTime > 5 * 60) {
					reload = true;
				}
				if(messageData === undefined || messageData === null || reload === true) {
					$.base.ajax({
						url: '/base/message',
						data: {language:language},
						success: function(data) {
							$.base.messageData = data['messageData'];
							sessionStorage.setItem($.base['messageDataKey'], JSON.stringify($.base.messageData));
							sessionStorage.setItem($.base['messagelanguageKey'], language);
							sessionStorage.setItem($.base['messageTimeKey'], Math.ceil(new Date().getTime() / 1000).toString());
						}
					});
				} else {
					$.base.messageData = JSON.parse(messageData);
				}
			}
			// 메세지가 없으면 Code를 출력한다.
			var result = undefined;
			if($.base.messageData === undefined || $.base.messageData[id] === undefined) {
				result = id;
			} else {
				result = $.base.messageData[id];
			}
			if($.isPlainObject(replacement)) {
				$.each(replacement, function(key) {
					var regExp = new RegExp(key, 'g');
					result = result.replace(regExp, replacement[key]);
				});
			}
			return result;
		},
		// Expire Session & Session Keep Alive
		expireTimer: function() {
			var expireTimer = {
                element: undefined,
                second: 0,
                startTime: 0,
                timeoutHandler: undefined,
                handlerIntervalMillis: 1000,
                keepAliveIntervalCount: 120
            };
			
			var $messageModal = undefined,
				showExpireNotice = false;
			
			var expireElement = document.getElementById('heade-timer-expire');
			if (expireElement) {
                expireTimer['element'] = $(expireElement);
                expireTimer['element'].parent('span').show();

                var initialTimes = expireTimer['element'].text().split(':');
                expireTimer['second'] = (parseInt(initialTimes[0], 10) * 60) + parseInt(initialTimes[1], 10);
            } else {
            	expireTimer['second'] = 30 * 60;
            }
			
			expireTimer['startTime'] = new Date().getTime();
			expireTimer['keepAliveLeftCount'] = expireTimer['keepAliveIntervalCount'];
            expireTimer['timeoutHandler'] = function () {
            	
            	// 모달이 실행중이면 타이머를 멈춘다.
            	if($('#message-modal').is(':visible') || $('#progress-modal').is(':visible')) {
            		expireTimer['startTime'] = expireTimer['startTime'] + expireTimer['handlerIntervalMillis'];
            	} else {
            		var currentTime = new Date().getTime(),
		                interval = Math.ceil((currentTime - expireTimer['startTime']) / expireTimer['handlerIntervalMillis']),
		                displayMinute = expireTimer['second'] - interval + 1;
            		
		            // 시간이 끝나면 로그아웃 실행
		            if(displayMinute < 1) {
		            	$.base.ajax({
		    				url: '/logout',
		    				success: function(data) {
		    					var redirectURL = '/'
		    					if($.base.isNotEmpty(data) && $.base.isNotEmpty(data['redirectURL'])) {
		    						redirectURL = data['redirectURL'];
		    					}
		    					location.href = redirectURL;
		    				}
		    			});
		            }
		            
		            // 1분 남았을 경우 모달로 메세지를 띄운다.
	                if(!showExpireNotice && displayMinute < 1 * 60) {
	                    showExpireNotice = true;
	                    $messageModal = $('#message-modal');
	                    
	                    var timerModalButtons = [];
	                    timerModalButtons.push('<button type="button" id="timer-close" class="btn btn-default">' + $.base.message('BUTTON.CLOSE') + '</button>');
	                    timerModalButtons.push('<button type="button" id="timer-extend" class="btn btn-primary">' + $.base.message('BUTTON.EXTEND') + '</button>');
	                    
	                    $messageModal.find('.modal-title').text($.base.message('TIMER.MODAL.TITLE'));
	                    $messageModal.find('.modal-body').html($.base.message('TIMER.MODAL.BODY'));
	                    $messageModal.find('.modal-footer').html(timerModalButtons.join(''));
	                    
	                    // Close 버튼 이벤트
	                    $messageModal.find('#timer-close').on('click', function(event) {
	                    	$messageModal.find('button').remove();
	                    	$messageModal.modal('hide');
	                    });
	                    
	                    // Extend 버튼 이벤트
	                    $messageModal.find('#timer-extend').on('click', function(event) {
	                    	showExpireNotice = false;
	                    	expireTimer['startTime'] = new Date().getTime();
	                    	$messageModal.find('#timer-close').click();
	                    });
	                    
	                    $messageModal.modal('show');
	                }
	                
	                // 실제 시간이 줄어드는 것을 화면에 표시
	                if (expireTimer['element'] && expireTimer['element'].length > 0) {
	                    var minute = Math.floor(displayMinute / 60),
	                        second = displayMinute % 60;
	                    displayMinute = (minute < 10 ? '0' : '') + minute + ':' + (second < 10 ? '0' : '') + second;
	                    expireTimer['element'].text(displayMinute);
	                    if($messageModal !== undefined) {
	                    	$messageModal.find('#expire-second').text(second);
	                    }
	                }
            	}
            	
            	// 세션 keep alive을 실행
            	if(expireTimer['keepAliveLeftCount']-- === 0) {
	            	expireTimer['keepAliveLeftCount'] = expireTimer['keepAliveIntervalCount'];
	            	
	            	// logout 버튼이 존재하면 정상적인 로그인 중이라고 가정.
                    $.base.ajax({
                        type: 'POST',
                        url: '/login/loginkeepalive',
                        data: {'time': displayMinute}
                    });
	            }
            };
            setInterval(expireTimer.timeoutHandler, expireTimer['handlerIntervalMillis']);
            
            $(document.body).on('click', '#header-timer-reset', function() {
            	expireTimer['startTime'] = new Date().getTime();
            });
	    }
	}
	
	/**
	 * 기본 동작 스크립트 실행
	 */
	$(document.body).ready(function() {
		
		// Set Moment Locale
		moment.locale($('meta[name="_locale"]').attr('content'));
		
		// Language
		$(this).on('click', '#header-language li', function(event) {
			var $li = $(event.currentTarget),
				language = $li.data('language');
			$.base.ajax({
				url: '/base/language-set',
				data: {'language':language},
				async: true,
				success: function(data) {
					location.reload();
				}
			});
		});
		
		// Login 페이지가 아니면 기본 실행하는 항목들
		if(window.location.pathname !== '/login') {
			// Expire 실행	
			$.base.expireTimer();
			
			// Logout
			$(this).on('click', '#header-logout', function() {
				$.base.ajax({
					url: '/logout',
					success: function(data) {
						var redirectURL = '/'
						if($.base.isNotEmpty(data) && $.base.isNotEmpty(data['redirectURL'])) {
							redirectURL = data['redirectURL'];
						}
						location.href = redirectURL;
					}
				});
			});
			
			// 페이지 이동시 Form(Editor) 수정 항목 체크
			$(window).on('beforeunload', function(){
				var changeData = $(document.body).data('changeData');
				if($.base.isNotEmpty(changeData)) {
					return 'Exists Change!!';
				}
			});
		}
		
	});
	
})(jQuery);