(function($) {
	'use strict';
	
	/**
	 * File Functions
	 */
	$.file = {
		// File Downloading Data Key
		FILE_PROCESSING: 'isFileProcessing',	
        // Download File
		download: function(options) {
			var downloadCompleteCookie = 'DOWNLOAD_COMPLETE_COOKIE',
				$modal = $('#progress-modal'),
				$progressbar = $modal.find('.progress-bar');
			
			if(typeof options === 'string') {
				options = {'url': options};
			}
			options = $.extend({'useProgress': true}, options);
			
			// 중복 동작 방지용
			if($(document.body).data($.file.FILE_PROCESSING) === true) {
				return false;
			} else {
				$(document.body).data($.file.FILE_PROCESSING, true);
			}
			
			deleteCookie(downloadCompleteCookie);
			$(document.body).data(downloadCompleteCookie, true);
			if(options['useProgress'] === true) {
				$progressbar.css('width', '100%').text('');
				$modal.modal('show');
				$modal.data('isDownloading', true);
			}
			
			// Request File Download
			var $form = $('<form style="display:none;"></form>'),
				$iframe = $('<iframe src="javascript:false;" name="iframe-download"></iframe>').bind('load', function() {
								$iframe.unbind('load').bind('load', function() {
									$('<iframe src="javascript:false;"></iframe>').appendTo($form);
									window.setTimeout(function() {
										$form.remove();
									}, 0);
								});
						  });
			$form.prop('action', options['url']);
			$form.prop('target', $iframe.prop('name'))
	        $form.prop('method', 'POST');
			$form.append('<input type="hidden" name="' + $('meta[name="_csrf_parameter"]').attr('content') + '" value="' + $('meta[name="_csrf"]').attr('content') + '">');
			if($.isPlainObject(options['data'])) {
				for(var key in options['data']) {
					$form.append('<input type="hidden" name="' + key + '" value="' + options['data'][key] + '">');
				}
			}
			$form.append($iframe).appendTo(document.body);
			$form.submit();
			
			// Check Cookie Interval
			var cookieCheckInterval = setInterval(function() {
				var result = getCookie(downloadCompleteCookie);
				if(result === 'success' || result === 'fail') {
					$(document.body).removeData(downloadCompleteCookie);
					deleteCookie(downloadCompleteCookie);
					clearInterval(cookieCheckInterval);
					$form.remove();
					$modal.modal('hide');
					if(result === 'fail') {
						// TODO Fail Notice
						alert('Download Fail');
					}
				} else if(typeof result === 'string' && result.split('::').length === 2) {
					var result = result.replace(/\"/g, ''),
						fileDownloadKey = result.split('::')[0],
						fileSize = result.split('::')[1];
					deleteCookie(downloadCompleteCookie);
					clearInterval(cookieCheckInterval);
					
					// Check Download Interval
					var progressShowing = false;
					var progressDownloadInterval = setInterval(function() {
						$.base.ajax({
							url: '/base/fileDownload',
							data: {'fileDownloadKey':fileDownloadKey},
							async: true,
							success: function(data) {
								var downloadSize = data['fileDownload']['size'],
									state = data['fileDownload']['state'];
								if(state === 'Success') {
									progressIntervalClose(true);
								} else if(state === 'Fail') {
									progressIntervalClose(false);
								} else if(state === 'Cancel') {
									progressIntervalClose(true);
								} else {
									// Show Progress
									if(progressShowing === false) {
										progressShowing = true;
										$progressbar.css('width', '0%').text('0%');
									}
									var percentage = Math.floor(downloadSize / fileSize * 100) + '%';
									$progressbar.css('width', percentage).text(percentage);
								}
							},
							error: function() {
								progressIntervalClose(false);
							}
						});
						
						function progressIntervalClose(isSuccess) {
							$form.remove();
							clearInterval(progressDownloadInterval);
							$(document.body).removeData($.file.FILE_PROCESSING);
							$modal.modal('hide');
							progressShowing = false;
							if(!isSuccess) {
								// TODO Fail Notice
							}
						}
					}, 2000);
				}
			}, 1000);
			return true;
			
			function setCookie(cookieName, cookieValue, cookieExpire, cookiePath, cookieDomain, cookieSecure){
			    var cookieText=escape(cookieName)+'='+escape(cookieValue);
			    cookieText+=(cookieExpire ? '; EXPIRES='+cookieExpire.toGMTString() : '');
			    cookieText+=(cookiePath ? '; PATH='+cookiePath : '');
			    cookieText+=(cookieDomain ? '; DOMAIN='+cookieDomain : '');
			    cookieText+=(cookieSecure ? '; SECURE' : '');
			    document.cookie=cookieText;
			}
			
			function getCookie(cookieName){
				var cookieValue=undefined;
				if(document.cookie){
					var array=document.cookie.split((escape(cookieName)+'=')); 
					if(array.length >= 2){
						var arraySub=array[1].split(';');
						cookieValue=unescape(arraySub[0]);
					}
				}
				return cookieValue;
			}
			 
			function deleteCookie(cookieName){
				var temp=getCookie(cookieName);
				if(temp){
					setCookie(cookieName,temp,(new Date(1)), '/');
				}
			}
		},
		// 파일 크기를 형식에 맞게 표현
		formatFileSize: function (bytes) {
            if(typeof bytes !== 'number') {
                return '';
            }
            if(bytes >= 1000000000) {
                return (bytes / 1000000000).toFixed(2) + ' GB';
            }
            if(bytes >= 1000000) {
                return (bytes / 1000000).toFixed(2) + ' MB';
            }
            return (bytes / 1000).toFixed(2) + ' KB';
        }
	}
	
	/**
	 * Regist File Upload 
	 * <input type="file"/> 에 jQuery File Upload 이벤트 실행
	 */
	$('input:file').fileupload({
		url: '/base/fileUpload',
		dataType: 'json',
		autoUpload: false,
		paramName: 'uploadFile',
		replaceFileInput: true,
		sequentialUploads: true,
		add: function(e, data) {
			var $target = $(e.target);
			$.each(data['files'], function(i, file) {
				var $nextAll = $target.nextAll(),
					$helpBlock = undefined,
					$fileTemplate = $('#fileTemplate').clone().removeAttr('id');

				$.each($target.nextAll(), function(i, next) {
					if($(next).hasClass('help-block')) {
						$helpBlock = $(next);
						return false;
					}
				});
				
				if($helpBlock === undefined) {
					$helpBlock = $('<div class="help-block"/>');
					$target.after($helpBlock);
				}
				
				$fileTemplate = $($.base.replaceTemplate($fileTemplate[0]['outerHTML'], {
					'fileName':file['name'], 'fileSize':$.file.formatFileSize(file['size'])
				})).data('file', data);
				
				// 취소버튼 활성화
				$fileTemplate.on('click', 'label.btn-danger', function(event) {
					$fileTemplate.remove();
				});
				
				$helpBlock.append($fileTemplate);
			});
			$target.val('');
		},
		progress: function(e, data) {
			var $fileElement = $(data['fileElement']),
				percentage = Math.floor(data.loaded / data.total * 100) + '%';
			$fileElement.find('.progress-bar').css('width', percentage).text(percentage);
		},
		done: function(e, data) {
			var $fileElement = $(data['fileElement']),
				result = data['result'];
			if($.base.isNotEmpty(result['fileKey'])) {
				$fileElement.find('input:hidden').attr('name', $(data['fileInput']).attr('name')).val(result['fileKey']);
				var completeFileCount = data['form'].data('completeFileCount');
				if(isNaN(completeFileCount)) {
					completeFileCount = 0;
				}
				completeFileCount = parseInt(completeFileCount) + 1;
				data['form'].data('completeFileCount', completeFileCount);
			} else {
				// TODO 에러
			}
		}
	});
	
})(jQuery)
